package com.example.c.helpou.Modelos;

import com.google.gson.annotations.SerializedName;

/**
 * Created by C on 05/11/2017.
 */

public class PojoCentroComercial {

    @SerializedName("idCentroComercial")
    int id_centro_comercial;
    @SerializedName("nombreCentroComercial")
    String nombre_centro_comercial;
    @SerializedName("latitud")
    float latitud_centro_comercial;
    @SerializedName("longitud")
    float longitud_centro_comercial;
    @SerializedName("ciudad")
    String ciudad_centro_comercial;
    @SerializedName("colonia")
    String colonia_centro_comercial;

    public int getId_centro_comercial() {
        return id_centro_comercial;
    }

    public void setId_centro_comercial(int id_centro_comercial) {
        this.id_centro_comercial = id_centro_comercial;
    }

    public String getNombre_centro_comercial() {
        return nombre_centro_comercial;
    }

    public void setNombre_centro_comercial(String nombre_centro_comercial) {
        this.nombre_centro_comercial = nombre_centro_comercial;
    }

    public float getLatitud_centro_comercial() {
        return latitud_centro_comercial;
    }

    public void setLatitud_centro_comercial(float latitud_centro_comercial) {
        this.latitud_centro_comercial = latitud_centro_comercial;
    }

    public float getLongitud_centro_comercial() {
        return longitud_centro_comercial;
    }

    public void setLongitud_centro_comercial(float longitud_centro_comercial) {
        this.longitud_centro_comercial = longitud_centro_comercial;
    }

    public String getCiudad_centro_comercial() {
        return ciudad_centro_comercial;
    }

    public void setCiudad_centro_comercial(String ciudad_centro_comercial) {
        this.ciudad_centro_comercial = ciudad_centro_comercial;
    }

    public String getColonia_centro_comercial() {
        return colonia_centro_comercial;
    }

    public void setColonia_centro_comercial(String colonia_centro_comercial) {
        this.colonia_centro_comercial = colonia_centro_comercial;
    }

    @Override
    public String toString() {
        return "PojoCentroComercial{" +
                "id_centro_comercial=" + id_centro_comercial +
                ", nombre_centro_comercial='" + nombre_centro_comercial + '\'' +
                ", latitud_centro_comercial=" + latitud_centro_comercial +
                ", longitud_centro_comercial=" + longitud_centro_comercial +
                ", ciudad_centro_comercial='" + ciudad_centro_comercial + '\'' +
                ", colonia_centro_comercial='" + colonia_centro_comercial + '\'' +
                '}';
    }
}
