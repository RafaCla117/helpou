package com.example.c.helpou.Modelos;

import com.google.gson.annotations.SerializedName;

/**
 * Created by C on 07/11/2017.
 */

public class PojoIdUsuario {

    @SerializedName("idUsuario")
    int id_usuario;

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    @Override
    public String toString() {
        return "PojoIdUsuario{" +
                "id_usuario=" + id_usuario +
                '}';
    }
}
