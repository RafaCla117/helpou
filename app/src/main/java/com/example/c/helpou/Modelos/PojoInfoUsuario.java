package com.example.c.helpou.Modelos;

/**
 * Created by C on 29/10/2017.
 */

public class PojoInfoUsuario {

    float id_usuario;
    String name_usuario;
    String ap_usuario;
    String am_usuario;
    String titulo_usuario;
    int sexo;

    public float getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(float id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getName_usuario() {
        return name_usuario;
    }

    public void setName_usuario(String name_usuario) {
        this.name_usuario = name_usuario;
    }

    public String getAp_usuario() {
        return ap_usuario;
    }

    public void setAp_usuario(String ap_usuario) {
        this.ap_usuario = ap_usuario;
    }

    public String getAm_usuario() {
        return am_usuario;
    }

    public void setAm_usuario(String am_usuario) {
        this.am_usuario = am_usuario;
    }

    public String getTitulo_usuario() {
        return titulo_usuario;
    }

    public void setTitulo_usuario(String titulo_usuario) {
        this.titulo_usuario = titulo_usuario;
    }

    public int getSexo() {
        return sexo;
    }

    public void setSexo(int sexo) {
        this.sexo = sexo;
    }
}
