package com.example.c.helpou.Modelos;

import com.google.gson.annotations.SerializedName;

/**
 * Created by stile on 25/10/2017.
 */

public class PojoDireccion {

    @SerializedName("idDirecciones")
    int id_dir_user;
    @SerializedName("pais")
    int id_pais_user;
    @SerializedName("estado")
    int id_edo_user;
    @SerializedName("municipio")
    int id_munic_user;
    @SerializedName("calles")
    int id_calles_user;
    @SerializedName("codigoPostal")
    int id_cp_user;
    @SerializedName("noInterior")
    int id_no_int_user;
    @SerializedName("noExterior")
    int id_no_ext_user;

    public int getId_dir_user() {
        return id_dir_user;
    }

    public void setId_dir_user(int id_dir_user) {
        this.id_dir_user = id_dir_user;
    }

    public int getId_pais_user() {
        return id_pais_user;
    }

    public void setId_pais_user(int id_pais_user) {
        this.id_pais_user = id_pais_user;
    }

    public int getId_edo_user() {
        return id_edo_user;
    }

    public void setId_edo_user(int id_edo_user) {
        this.id_edo_user = id_edo_user;
    }

    public int getId_munic_user() {
        return id_munic_user;
    }

    public void setId_munic_user(int id_munic_user) {
        this.id_munic_user = id_munic_user;
    }

    public int getId_calles_user() {
        return id_calles_user;
    }

    public void setId_calles_user(int id_calles_user) {
        this.id_calles_user = id_calles_user;
    }

    public int getId_cp_user() {
        return id_cp_user;
    }

    public void setId_cp_user(int id_cp_user) {
        this.id_cp_user = id_cp_user;
    }

    public int getId_no_int_user() {
        return id_no_int_user;
    }

    public void setId_no_int_user(int id_no_int_user) {
        this.id_no_int_user = id_no_int_user;
    }

    public int getId_no_ext_user() {
        return id_no_ext_user;
    }

    public void setId_no_ext_user(int id_no_ext_user) {
        this.id_no_ext_user = id_no_ext_user;
    }
}
