package com.example.c.helpou.Retrofit;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.example.c.helpou.Fragments.FragmentsInterfaces.ArtMasCompInterface;
import com.example.c.helpou.Fragments.FragmentsInterfaces.AsistenteComprasInterface;
import com.example.c.helpou.Fragments.FragmentsInterfaces.ComparaBuscaInterface;
import com.example.c.helpou.Fragments.FragmentsInterfaces.GastosPerdioInterface;
import com.example.c.helpou.Fragments.FragmentsInterfaces.HomeFragmentInterface;
import com.example.c.helpou.Fragments.FragmentsInterfaces.ListaProdInterface;
import com.example.c.helpou.Fragments.FragmentsInterfaces.MainActivityInterface;
import com.example.c.helpou.Modelos.PojoCentroComercial;
import com.example.c.helpou.Modelos.PojoGetListItems;
import com.example.c.helpou.Modelos.PojoIdUsuario;
import com.example.c.helpou.Modelos.PojoListaProductos;
import com.example.c.helpou.Modelos.PojoPassworRecover;
import com.example.c.helpou.Modelos.PojoPresupuestoCompra;
import com.example.c.helpou.Modelos.PojoRecuperarContraseña;
import com.example.c.helpou.Modelos.PojoRequestItemsByDate;
import com.example.c.helpou.Modelos.PojoRequestTitle;
import com.example.c.helpou.Modelos.PojoResponseGetTitle;
import com.example.c.helpou.Modelos.PojoUsuario;
import com.example.c.helpou.Modelos.PojoUsuarioSignIn;
import com.example.c.helpou.Retrofit.Interfaces.CallerService;
import com.example.c.helpou.Views.ViesInterfaces.MapsActivityInterface;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by C on 08/10/2017.
 */

public class RestApi {
    private String TAG = "TAG";
    private static Retrofit retrofit = null;
    private static String URL_BASE = "http://8.120.0.129:8888/";
    CallerService callerService;
    Context context;

    public RestApi(Context context) {
        this.context = context;
        callerService = getInstance().create(CallerService.class);
    }

    public static Retrofit getInstance() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(URL_BASE)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public void insertaUsuario(PojoUsuario pojo_usuario) {

        Call<Object> call = callerService.setNewUsuario(pojo_usuario);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Log.i(TAG, "in function insertaUsuario(), onResponse: " + response.body());
                Toast toast1 = Toast.makeText(context, "Usuario registrado ;)", Toast.LENGTH_SHORT);
                toast1.show();
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.e(TAG, "in function insertaUsuario(), onFailure: " + t);
                Toast toast1 = Toast.makeText(context, "Usuario no registrado intenta de nuevo ;)", Toast.LENGTH_SHORT);
                toast1.show();
            }
        });

    }

    public void autenticarUsuario(PojoUsuarioSignIn usuario_sign_in, final MainActivityInterface main_act_interface) {

        Call<Object> call = callerService.checkUser(usuario_sign_in);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {

                Log.i(TAG, "en la funcion autenticarUsuario(), onResponse: " + response.body());

                String respuesta = "" + response.body();

                Log.i(TAG, "la respuesta es: " + respuesta);

                if (respuesta.equals("-1.0")) {
                    Toast toast1 = Toast.makeText(context, "E-mail o contraseña incorrectos porfavor verifica ;)", Toast.LENGTH_SHORT);
                    toast1.show();
                } else {
                    respuesta.replaceAll("^[\\p{L} .-]+$", "");

                    String[] info = respuesta.split(",");

                    info[0] = info[0].substring(1, info[0].length());
                    info[5] = info[5].substring(0, info[5].length() - 1);

                    String user_data = "";

                    for (int i = 0; i < info.length; i++) {
                        user_data = user_data + info[i] + ",";
                    }

                    Log.i(TAG, "onResponse() data que envio " + user_data);
                    main_act_interface.ValidUserDone(user_data);

                }

            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.e(TAG, "in functuin autenticarUsuario(), onFailure: " + t);
                Toast toast1 = Toast.makeText(context, "No se puede conectar con el servidor porfavor verifica tu conexión ;)", Toast.LENGTH_SHORT);
                toast1.show();
            }
        });

    }

    public void getPassword(PojoRecuperarContraseña correo_usuario, final MainActivityInterface main_act_interface) {

        Call<Object> call = callerService.recoverPassword(correo_usuario);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {

                Log.i(TAG, "en la funcion getPassword() respuesta de el serivdor: " + response.body());

                String respuesta = "" + response.body();

                if (respuesta.equals("-1.0")) {
                    Toast toast1 = Toast.makeText(context, "E-mail no registrado porfavor verifica ;)", Toast.LENGTH_SHORT);
                    toast1.setGravity(Gravity.CENTER, 50, 100);
                    toast1.show();
                    main_act_interface.exceptionEmal();
                } else {

                    Gson gson = new Gson();
                    PojoPassworRecover passwor_recovered = gson.fromJson(respuesta, PojoPassworRecover.class);

                    main_act_interface.sendEmailForgotPass(passwor_recovered.getPassword_recover());
                }

            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
            }
        });

    }

    public void getTitleUser(PojoRequestTitle pojoRequestTitle, final HomeFragmentInterface homeFragmentInterface) {
        Call<PojoResponseGetTitle> call = callerService.getTitleUser(pojoRequestTitle);
        call.enqueue(new Callback<PojoResponseGetTitle>() {
            @Override
            public void onResponse(Call<PojoResponseGetTitle> call, Response<PojoResponseGetTitle> response) {
                Log.i(TAG, "en la funcion getTitleUser() respuesta del servidor " + response.body());
                PojoResponseGetTitle pojoResponseGetTitle = response.body();

                homeFragmentInterface.paintProgress(pojoResponseGetTitle.getNumero_compras());

            }

            @Override
            public void onFailure(Call<PojoResponseGetTitle> call, Throwable t) {

            }
        });
    }

    public void setPresupuestoCompra(PojoPresupuestoCompra presupuesto_usuario, final AsistenteComprasInterface interface_asitente_compras) {
        Call<Object> call = callerService.setPresupuestoUsuario(presupuesto_usuario);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Log.i(TAG, "en la funcion setPresupuestoCompra(), respuesta del servidor: " + response.body());
                String respuesta = "" + response.body();
                interface_asitente_compras.setNewListItems(respuesta);
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.e(TAG, "En la funcion setPresupuestoCompra() fallo la conexion con el servidor");
                Log.d(TAG, "onFailure() returned: " + t);
            }
        });
    }

    public void setListaProductos(PojoListaProductos lista_productos, final AsistenteComprasInterface interface_asistente) {

        Call<Object> call = callerService.setListaProductos(lista_productos);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Log.i(TAG, "en la funcion setListaProductos(), lista de productos enviada");
                interface_asistente.listSended();
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {

            }
        });

    }

    public void getLocationsSuper(final MapsActivityInterface maps_act_interface) {
        Call<List<PojoCentroComercial>> call = callerService.getLocationsCentroComercial();
        call.enqueue(new Callback<List<PojoCentroComercial>>() {
            @Override
            public void onResponse(Call<List<PojoCentroComercial>> call, Response<List<PojoCentroComercial>> response) {
                Log.i(TAG, "getLocationsSuper(), respuesta del servidor: " + response.body());
                maps_act_interface.paintSuper(response.body());
            }

            @Override
            public void onFailure(Call<List<PojoCentroComercial>> call, Throwable t) {

            }
        });
    }

    public void getListArtMasComprado(PojoIdUsuario pojo_id_usuario, final ArtMasCompInterface art_mas_comp_interf) {
        Call<Object> call = callerService.getListArtMasComprado(pojo_id_usuario);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Log.i(TAG, "onResponse() Json recibido: " + response.body());
                art_mas_comp_interf.listaArtMasComprados("" + response.body());
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {

            }
        });

    }

    public void getGastosPeriodoTiempo(PojoIdUsuario pojoIdUsuario, final GastosPerdioInterface gastosPerdioInterface) {

        Call<Object> call = callerService.getListGastosPeriodoTiempo(pojoIdUsuario);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                //Log.i(TAG, "onResponse: gastos recibidos: " + response.body());
                gastosPerdioInterface.paintData("" + response.body());
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {

            }
        });

    }

    public void getListaFechasListasP(PojoIdUsuario pojoIdUsuario, final ListaProdInterface listaProdInterface) {
        Call<Object> call = callerService.getListFechasListasProd(pojoIdUsuario);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Log.i(TAG, "onResponse: Lista de fecchas obtenidas " + response.body());
                listaProdInterface.setListFechas("" + response.body());
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {

            }
        });
    }

    public void getItemsByDate(PojoRequestItemsByDate pojoRequestItemsByDate, final ListaProdInterface listaProdInterface) {
        Call<Object> call = callerService.getProdPorFecha(pojoRequestItemsByDate);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Log.i(TAG, "getItemsByDate() Respuesta del servidor: " + response.body());
                listaProdInterface.setTableContent("" + response.body());
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {

            }
        });
    }

    public void getListItems(PojoGetListItems pojoGetListItems, final ComparaBuscaInterface comparaBuscaInterface) {
        Call<Object> call = callerService.getAllItems(pojoGetListItems);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                comparaBuscaInterface.setListItems("" + response.body());
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {

            }
        });
    }
}
