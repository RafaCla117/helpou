package com.example.c.helpou.Views;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.example.c.helpou.Fragments.FragmentsInterfaces.MainActivityInterface;
import com.example.c.helpou.Modelos.PojoCentroComercial;
import com.example.c.helpou.R;
import com.example.c.helpou.Retrofit.RestApi;
import com.example.c.helpou.Views.ViesInterfaces.MapsActivityInterface;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, MapsActivityInterface {

    private GoogleMap mMap;
    private Marker marker;
    private double longitude_GPS, latitud_GPS;
    private RestApi caller_service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        caller_service = new RestApi(this);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        caller_service.getLocationsSuper(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setBuildingsEnabled(true);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);

    }

    public void addMarker(double lat, double lng, String title, String snippet) {
        LatLng coordenadas = new LatLng(lat, lng);
        CameraUpdate miUbicacion = CameraUpdateFactory.newLatLngZoom(coordenadas, 16);

        marker = mMap.addMarker(new MarkerOptions()
                .position(coordenadas)
                .title(title)
                .snippet(snippet)
        );
        //mMap.animateCamera(miUbicacion);
    }


    @Override
    public void paintSuper(List<PojoCentroComercial> list_centros) {
        for (int i = 0; i < list_centros.size(); i++) {
            addMarker(list_centros.get(i).getLatitud_centro_comercial(),
                    list_centros.get(i).getLongitud_centro_comercial(),
                    list_centros.get(i).getNombre_centro_comercial(),
                    list_centros.get(i).getColonia_centro_comercial());
        }
    }
}
