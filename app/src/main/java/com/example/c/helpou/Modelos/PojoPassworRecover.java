package com.example.c.helpou.Modelos;

import com.google.gson.annotations.SerializedName;

/**
 * Created by C on 04/11/2017.
 */

public class PojoPassworRecover {

    @SerializedName("pass")
    String password_recover;

    public String getPassword_recover() {
        return password_recover;
    }

    public void setPassword_recover(String password_recover) {
        this.password_recover = password_recover;
    }

    @Override
    public String toString() {
        return "PojoPassworRecover{" +
                "password_recover='" + password_recover + '\'' +
                '}';
    }
}
