package com.example.c.helpou.Modelos;

import com.google.gson.annotations.SerializedName;

/**
 * Created by C on 05/11/2017.
 */

public class PojoRequestTitle {

    @SerializedName("idUsuario")
    String id_usuario;

    public String getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(String id_usuario) {
        this.id_usuario = id_usuario;
    }

    @Override
    public String toString() {
        return "PojoRequestTitle{" +
                "id_usuario=" + id_usuario +
                '}';
    }
}
