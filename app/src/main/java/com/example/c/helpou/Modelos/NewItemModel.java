package com.example.c.helpou.Modelos;

import java.io.Serializable;

/**
 * Created by C on 18/10/2017.
 */

public class NewItemModel implements Serializable {

    String name_item;
    String brand_item;
    String filing_item;
    int quantity_item;
    float cost_item;

    public String getName_item() {
        return name_item;
    }

    public void setName_item(String name_item) {
        this.name_item = name_item;
    }

    public String getBrand_item() {
        return brand_item;
    }

    public void setBrand_item(String brand_item) {
        this.brand_item = brand_item;
    }

    public String getFiling_item() {
        return filing_item;
    }

    public void setFiling_item(String filing_item) {
        this.filing_item = filing_item;
    }

    public float getCost_item() {
        return cost_item;
    }

    public void setCost_item(float cost_item) {
        this.cost_item = cost_item;
    }

    public int getQuantity_item() {
        return quantity_item;
    }

    public void setQuantity_item(int quantity_item) {
        this.quantity_item = quantity_item;
    }

    @Override
    public String toString() {
        return "NewItemModel{" +
                "name_item='" + name_item + '\'' +
                ", brand_item='" + brand_item + '\'' +
                ", filing_item='" + filing_item + '\'' +
                ", quantity_item=" + quantity_item +
                ", cost_item=" + cost_item +
                '}';
    }
}
