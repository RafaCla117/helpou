package com.example.c.helpou.Retrofit.Interfaces;

import com.example.c.helpou.Modelos.PojoCentroComercial;
import com.example.c.helpou.Modelos.PojoGetListItems;
import com.example.c.helpou.Modelos.PojoIdUsuario;
import com.example.c.helpou.Modelos.PojoListaProductos;
import com.example.c.helpou.Modelos.PojoPresupuestoCompra;
import com.example.c.helpou.Modelos.PojoRecuperarContraseña;
import com.example.c.helpou.Modelos.PojoRequestItemsByDate;
import com.example.c.helpou.Modelos.PojoRequestTitle;
import com.example.c.helpou.Modelos.PojoResponseGetTitle;
import com.example.c.helpou.Modelos.PojoRespuestaUsuario;
import com.example.c.helpou.Modelos.PojoUsuario;
import com.example.c.helpou.Modelos.PojoUsuarioSignIn;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by C on 08/10/2017.
 */

public interface CallerService {

    @Headers("Content-Type: application/json")
    @POST("WS16/webresources/user/saveDataUser")
    Call<Object> setNewUsuario(@Body PojoUsuario pojo_usuario);

    @Headers("Content-Type: application/json")
    @POST("WS16/webresources/user/logIn")
    Call<Object> checkUser(@Body PojoUsuarioSignIn usuario);

    @Headers("Content-Type: application/json")
    @POST("WS16/webresources/user/recoverPass")
    Call<Object> recoverPassword(@Body PojoRecuperarContraseña correo_usuario);

    @Headers("Content-Type: application/json")
    @POST("WS16/webresources/user/numeroCompras")
    Call<PojoResponseGetTitle> getTitleUser(@Body PojoRequestTitle correo_usuario);

    @Headers("Content-Type: application/json")
    @POST("WS16/webresources/estimadoGasto/saveDataEstimadoGasto")
    Call<Object> setPresupuestoUsuario(@Body PojoPresupuestoCompra presupuesto_usuario);

    @Headers("Content-Type: application/json")
    @POST("WS16/webresources/listaProductos/saveListProduct")
    Call<Object> setListaProductos(@Body PojoListaProductos pojo_lista_prod);

    @GET("WS16/webresources/com.helpou.service.centrocomercial/all")
    Call<List<PojoCentroComercial>> getLocationsCentroComercial();

    @Headers("Content-Type: application/json")
    @POST("WS16/webresources/Historicos/articuloMasComprado")
    Call<Object> getListArtMasComprado(@Body PojoIdUsuario pojo_id_usuario);

    @Headers("Content-Type: application/json")
    @POST("WS16/webresources/Historicos/obtenerGastoPorPeriodoTiempo")
    Call<Object> getListGastosPeriodoTiempo(@Body PojoIdUsuario pojo_id_usuario);

    @Headers("Content-Type: application/json")
    @POST("WS16/webresources/Historicos/fechasListasProductos")
    Call<Object> getListFechasListasProd(@Body PojoIdUsuario pojo_id_usuario);

    @Headers("Content-Type: application/json")
    @POST("WS16/webresources/Historicos/obtener_lista_productos_por_fecha")
    Call<Object> getProdPorFecha(@Body PojoRequestItemsByDate pojo_id_usuario);

    @Headers("Content-Type: application/json")
    @POST("WS16/webresources/ProductoDump/compararProducto")
    Call<Object> getAllItems(@Body PojoGetListItems pojo_item);
}
