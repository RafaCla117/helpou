package com.example.c.helpou.Fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.c.helpou.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class HistoricosFragment extends Fragment {

    private static final String TAG = "TAG - HistoricosFragment ";
    private View fragmentView;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private String datos_user;
    private int[] tabIcons = {
            R.drawable.icon_lista_prod,
            R.drawable.icon_prod_mconmprado,
            R.drawable.icon_gastos_toempo,
            R.drawable.icon_sup_mas_visitado,
            R.drawable.icon_alsa_prec
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_historicos, container, false);

        toolbar = (Toolbar) fragmentView.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        viewPager = (ViewPager) fragmentView.findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) fragmentView.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        return fragmentView;
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        tabLayout.getTabAt(3).setIcon(tabIcons[3]);
        tabLayout.getTabAt(4).setIcon(tabIcons[4]);

    }

    public void setDatos_user(String datos_user) {
        this.datos_user = "[" + datos_user + "]";
        separateDataUser(this.datos_user);

    }

    public void separateDataUser(String jsonString) {

        Log.i(TAG, " onCreateView() salida de json: " + jsonString);

        ArrayList<String> stringArray = new ArrayList<>();

        JSONArray jsonArray;

        try {
            jsonArray = new JSONArray(jsonString);

            for (int i = 0; i < jsonArray.length(); i++) {
                stringArray.add(jsonArray.getString(i));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager());

        ListaProdFragment list_prod_frag = new ListaProdFragment();
        ArtMasCompFragmen art_mas_comp_frag = new ArtMasCompFragmen();
        AlzaPreciosFragment alza_prec_frag = new AlzaPreciosFragment();
        SuperMasVisitFragment super_mas_visit_frag = new SuperMasVisitFragment();
        GastosPeriodoFragment gastos_periodo_frag = new GastosPeriodoFragment();

        art_mas_comp_frag.setDatos_user(datos_user);
        gastos_periodo_frag.setDatos_usuario(datos_user);
        list_prod_frag.setDatos_user(datos_user);

        adapter.addFragment(list_prod_frag, "Lista productos");
        adapter.addFragment(art_mas_comp_frag, "Artículo mas comprado");
        adapter.addFragment(alza_prec_frag, "Alza de precios");
        adapter.addFragment(super_mas_visit_frag, "Supermercado mas visitado");
        adapter.addFragment(gastos_periodo_frag, "Gastos por periodo");

        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }

}
