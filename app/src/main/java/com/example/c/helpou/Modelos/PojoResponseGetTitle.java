package com.example.c.helpou.Modelos;

import com.google.gson.annotations.SerializedName;

/**
 * Created by C on 05/11/2017.
 */

public class PojoResponseGetTitle {

    @SerializedName("numero_compras")
    int numero_compras;

    public int getNumero_compras() {
        return numero_compras;
    }

    public void setNumero_compras(int numero_compras) {
        this.numero_compras = numero_compras;
    }

    @Override
    public String toString() {
        return "PojoResponseGetTitle{" +
                "numero_compras=" + numero_compras +
                '}';
    }
}
