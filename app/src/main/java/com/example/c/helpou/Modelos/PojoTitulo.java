package com.example.c.helpou.Modelos;

import com.google.gson.annotations.SerializedName;

/**
 * Created by C on 08/10/2017.
 */

public class PojoTitulo {

    @SerializedName("idTitulo")
    int id_titulo;
    @SerializedName("nombreTitulo")
    String nombre_titulo;
    @SerializedName("descripcionTitulo")
    String descripcion_titulo;

    public int getId_titulo() {
        return id_titulo;
    }

    public void setId_titulo(int id_titulo) {
        this.id_titulo = id_titulo;
    }

    public String getNombre_titulo() {
        return nombre_titulo;
    }

    public void setNombre_titulo(String nombre_titulo) {
        this.nombre_titulo = nombre_titulo;
    }

    public String getDescripcion_titulo() {
        return descripcion_titulo;
    }

    public void setDescripcion_titulo(String descripcion_titulo) {
        this.descripcion_titulo = descripcion_titulo;
    }

    @Override
    public String toString() {
        return "PojoTitulo{" +
                "id_titulo=" + id_titulo +
                ", nombre_titulo='" + nombre_titulo + '\'' +
                ", descripcion_titulo='" + descripcion_titulo + '\'' +
                '}';
    }
}
