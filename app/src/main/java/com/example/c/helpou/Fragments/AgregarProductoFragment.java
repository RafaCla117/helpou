package com.example.c.helpou.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.c.helpou.Fragments.FragmentsInterfaces.AsistenteComprasInterface;
import com.example.c.helpou.Modelos.NewItemModel;
import com.example.c.helpou.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AgregarProductoFragment extends Fragment implements View.OnClickListener {

    String TAG = "TAG";
    private View fragmmentView;
    EditText jvedt_nom_prod, jvedt_marca_prod, jvedt_pres_prod, jvedt_precio_prod, jvedt_cantidad_prod;
    Button jvbtn_add_item, jvbtn_close;
    AsistenteComprasInterface asistente_comp_interface;
    boolean exist_item = false;
    NewItemModel item_to_updated;
    int cantidad_prod;
    float user_budget, item_cost;


    public void setAsistente_comp_interface(AsistenteComprasInterface asistente_comp_interface) {
        this.asistente_comp_interface = asistente_comp_interface;
    }

    public boolean validatNewItemsFiels() {

        String field1 = "" + jvedt_nom_prod.getText();
        String field2 = "" + jvedt_marca_prod.getText();
        String field3 = "" + jvedt_pres_prod.getText();
        String field4 = "" + jvedt_cantidad_prod.getText();
        String field5 = "" + jvedt_precio_prod.getText();
        boolean flag = true;

        if (field1.equals("") || !validateString(field1)) {
            Toast toast1 = Toast.makeText(fragmmentView.getContext(), "Nombre de producto vacio o no valido porfavor verifica  ;)", Toast.LENGTH_LONG);
            toast1.show();
            flag = false;
        } else if (field2.equals("") || !validateString(field2)) {
            Toast toast1 = Toast.makeText(fragmmentView.getContext(), "Marca de producto vacia o no valida porfavor verifica  ;)", Toast.LENGTH_LONG);
            toast1.show();
            flag = false;
        } else if (field3.equals("")) {
            Toast toast1 = Toast.makeText(fragmmentView.getContext(), "Presentación de producto vacia o no valida porfavor verifica  ;)", Toast.LENGTH_LONG);
            toast1.show();
            flag = false;
        } else if (field4.equals("")) {
            Toast toast1 = Toast.makeText(fragmmentView.getContext(), "Cantidad de producto vacia porfavor verifica  ;)", Toast.LENGTH_LONG);
            toast1.show();
            flag = false;
        } else if (field5.equals("")) {
            Toast toast1 = Toast.makeText(fragmmentView.getContext(), "Precio de producto vacio porfavor verifica  ;)", Toast.LENGTH_LONG);
            toast1.show();
            flag = false;
        }

        cantidad_prod = Integer.parseInt("" + jvedt_cantidad_prod.getText());
        item_cost = Float.parseFloat("" + jvedt_precio_prod.getText()) * cantidad_prod;

        return flag;
    }

    public NewItemModel getNewItem() {
        NewItemModel new_item = new NewItemModel();
        new_item.setName_item("" + jvedt_nom_prod.getText());
        new_item.setBrand_item("" + jvedt_marca_prod.getText());
        new_item.setFiling_item("" + jvedt_pres_prod.getText());
        new_item.setQuantity_item(cantidad_prod);
        new_item.setCost_item(item_cost);
        return new_item;
    }

    public void updateItem(NewItemModel item_to_updated, boolean exist_item) {
        this.exist_item = exist_item;
        this.item_to_updated = item_to_updated;
    }

    public boolean validateString(String s) {

        if (s == null || s.trim().isEmpty()) {
            return false;
        }

        Pattern p = Pattern.compile("^[\\p{L} .-]+$");
        Matcher m = p.matcher(s);

        boolean b = m.find();

        return b;
    }

    public void setUser_budget(float user_budget) {
        this.user_budget = user_budget;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmmentView = inflater.inflate(R.layout.fragment_agregar_producto, container, false);
        jvedt_nom_prod = fragmmentView.findViewById(R.id.xml_edtxt_nom_prod);
        jvedt_marca_prod = fragmmentView.findViewById(R.id.xml_edtxt_marc_prod);
        jvedt_pres_prod = fragmmentView.findViewById(R.id.xml_edtxt_present_prod);
        jvedt_precio_prod = fragmmentView.findViewById(R.id.xlm_edtxt_precio_prod);
        jvbtn_add_item = fragmmentView.findViewById(R.id.xml_btn_ing_new_item);
        jvedt_cantidad_prod = fragmmentView.findViewById(R.id.xml_edtxt_cantidad_prod);
        jvbtn_close = fragmmentView.findViewById(R.id.xml_btn_close_form_new_item);

        jvbtn_add_item.setOnClickListener(this);
        jvbtn_close.setOnClickListener(this);

        if (exist_item) {
            jvedt_nom_prod.setText(item_to_updated.getName_item());
            jvedt_marca_prod.setText(item_to_updated.getBrand_item());
            jvedt_pres_prod.setText(item_to_updated.getFiling_item());
        }

        return fragmmentView;
    }

    @Override
    public void onClick(View view) {

        int opc = view.getId();

        switch (opc) {
            case R.id.xml_btn_ing_new_item:
                if (validatNewItemsFiels()) {
                    if (user_budget >= item_cost) {
                        asistente_comp_interface.addNewItem(getNewItem());
                        getActivity().getSupportFragmentManager().popBackStack();
                    } else {
                        Toast toast1 = Toast.makeText(fragmmentView.getContext(), "Este producto supera tu presupuesto no se puede agregar ;)", Toast.LENGTH_SHORT);
                        toast1.show();
                    }
                }
                break;
            case R.id.xml_btn_close_form_new_item:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }

    }

}
