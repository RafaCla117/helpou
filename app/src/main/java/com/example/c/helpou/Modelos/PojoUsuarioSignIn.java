package com.example.c.helpou.Modelos;

import com.google.gson.annotations.SerializedName;

/**
 * Created by C on 29/10/2017.
 */

public class PojoUsuarioSignIn {

    @SerializedName("correo")
    String correo_user;
    @SerializedName("pwd")
    String pwd;

    public String getCorreo_user() {
        return correo_user;
    }

    public void setCorreo_user(String correo_user) {
        this.correo_user = correo_user;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    @Override
    public String toString() {
        return "PojoUsuarioSignIn{" +
                "correo_user='" + correo_user + '\'' +
                ", pwd='" + pwd + '\'' +
                '}';
    }
}
