package com.example.c.helpou.Modelos;

import android.content.Intent;

import com.google.gson.annotations.SerializedName;

/**
 * Created by C on 06/11/2017.
 */

public class PojoProducto {

    @SerializedName("idProductos")
    int id_producto;
    @SerializedName("marca")
    PojoMarcaProducto marca_prod;
    @SerializedName("presentacion")
    PojoPresentacionProducto presentacion_producto;
    @SerializedName("unidadDeMedida")
    PojoUnidadMedida unidad_medida_producto;
    @SerializedName("nombre")
    String nombre_producto;
    @SerializedName("precioProducto")
    String precio_producto;
    @SerializedName("cantidad")
    String cantidad_producto;

    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public PojoMarcaProducto getMarca_prod() {
        return marca_prod;
    }

    public void setMarca_prod(PojoMarcaProducto marca_prod) {
        this.marca_prod = marca_prod;
    }

    public PojoPresentacionProducto getPresentacion_producto() {
        return presentacion_producto;
    }

    public void setPresentacion_producto(PojoPresentacionProducto presentacion_producto) {
        this.presentacion_producto = presentacion_producto;
    }

    public PojoUnidadMedida getUnidad_medida_producto() {
        return unidad_medida_producto;
    }

    public void setUnidad_medida_producto(PojoUnidadMedida unidad_medida_producto) {
        this.unidad_medida_producto = unidad_medida_producto;
    }

    public String getNombre_producto() {
        return nombre_producto;
    }

    public void setNombre_producto(String nombre_producto) {
        this.nombre_producto = nombre_producto;
    }

    public String getPrecio_producto() {
        return precio_producto;
    }

    public void setPrecio_producto(String precio_producto) {
        this.precio_producto = precio_producto;
    }

    public String getCantidad_producto() {
        return cantidad_producto;
    }

    public void setCantidad_producto(String cantidad_producto) {
        this.cantidad_producto = cantidad_producto;
    }

    @Override
    public String toString() {
        return "PojoProducto{" +
                "id_producto='" + id_producto + '\'' +
                ", marca_prod=" + marca_prod +
                ", presentacion_producto=" + presentacion_producto +
                ", unidad_medida_producto=" + unidad_medida_producto +
                ", nombre_producto='" + nombre_producto + '\'' +
                ", precio_producto='" + precio_producto + '\'' +
                ", cantidad_producto='" + cantidad_producto + '\'' +
                '}';
    }
}
