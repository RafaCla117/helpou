package com.example.c.helpou.Modelos;

import com.google.gson.annotations.SerializedName;

/**
 * Created by C on 29/10/2017.
 */

public class PojoRespuestaUsuario {

    PojoInfoUsuario pojo_info_usuario[];

    public PojoInfoUsuario[] getPojo_info_usuario() {
        return pojo_info_usuario;
    }

    public void setPojo_info_usuario(PojoInfoUsuario[] pojo_info_usuario) {
        this.pojo_info_usuario = pojo_info_usuario;
    }
}
