package com.example.c.helpou.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.format.Time;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.c.helpou.Fragments.FragmentsInterfaces.AsistenteComprasInterface;
import com.example.c.helpou.Modelos.NewItemModel;
import com.example.c.helpou.Modelos.PojoCentroComercial;
import com.example.c.helpou.Modelos.PojoListaProductos;
import com.example.c.helpou.Modelos.PojoMarcaProducto;
import com.example.c.helpou.Modelos.PojoPresentacionProducto;
import com.example.c.helpou.Modelos.PojoPresupuestoCompra;
import com.example.c.helpou.Modelos.PojoProducto;
import com.example.c.helpou.Modelos.PojoTitulo;
import com.example.c.helpou.Modelos.PojoUnidadMedida;
import com.example.c.helpou.Modelos.PojoUsuario;
import com.example.c.helpou.R;
import com.example.c.helpou.Retrofit.RestApi;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AsistenteCompras extends Fragment implements AsistenteComprasInterface, View.OnClickListener {

    private String TAG = "TAG";
    private View fragmentView;
    private float user_budget = 0.0f, initial_budget;
    private String supermercado_usuario;
    private TextView jv_txt_user_final_budget;
    private Button delete_item;
    private LinearLayout table_row;
    private TableLayout jv_table_list_items;
    private TableRow mTableRow, selected_table_row;
    private Button jvbtn_launch_form_new_item, jvbtn_fin_asist_compras;
    private LayoutInflater layoutInflater;
    private RestApi service_caller;
    private String datos_usuario;
    private String fecha_compra;
    private ArrayList<NewItemModel> contentTable = new ArrayList<>();
    private ProgressDialog progress;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_asistente_compras, container, false);
        jv_txt_user_final_budget = fragmentView.findViewById(R.id.text_user_final_budget_view);
        jv_table_list_items = fragmentView.findViewById(R.id.xml_table_list_items);
        jvbtn_launch_form_new_item = fragmentView.findViewById(R.id.xml_launch_form_new_item);
        jvbtn_fin_asist_compras = fragmentView.findViewById(R.id.xml_btn_fin_asistente_comp);
        service_caller = new RestApi(getContext());

        layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        table_row = (LinearLayout) layoutInflater.inflate(R.layout.items_row_table, null, false);
        table_row.setBackgroundDrawable(getResources().getDrawable(R.color.colorPrimaryDark));

        jvbtn_launch_form_new_item.setOnClickListener(this);
        jvbtn_fin_asist_compras.setOnClickListener(this);
        progress = new ProgressDialog(getContext());
        progress.setMessage("Enviando datos, por favor espere...");

        jv_txt_user_final_budget.setText("" + user_budget);

        return fragmentView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStart() {
        super.onStart();
        updateTable();
    }

    @Override
    public void addNewItem(NewItemModel new_item) {
        user_budget = user_budget - new_item.getCost_item();
        jv_txt_user_final_budget.setText("" + user_budget);
        contentTable.add(new_item);
    }

    @Override
    public void setNewListItems(String id_compra) {

        PojoListaProductos lista_productos_usuario = new PojoListaProductos();

        String[] datos_servidor = id_compra.split(",");
        datos_servidor[0] = datos_servidor[0].substring(1, datos_servidor[0].length());
        datos_servidor[1] = datos_servidor[1].substring(0, datos_servidor[1].length() - 1);

        float id_estimado_gasto = Float.parseFloat(datos_servidor[0]);
        float id_supermercado = Float.parseFloat(datos_servidor[1]);

        List<PojoProducto> list_pojo_productos = new ArrayList<>();

        for (int i = 0; i < contentTable.size(); i++) {

            PojoProducto pojo_producto = new PojoProducto();
            pojo_producto.setId_producto(i + 1);

            PojoMarcaProducto marca_producto = new PojoMarcaProducto();
            marca_producto.setId_marca_producto(1);
            marca_producto.setNombre_marca(contentTable.get(i).getBrand_item());
            marca_producto.setClave_producto("a1");
            marca_producto.setDescripcion_poducto("--");
            pojo_producto.setMarca_prod(marca_producto);

            PojoPresentacionProducto presentacion_producto = new PojoPresentacionProducto();
            presentacion_producto.setId_tipo_presentacion(1);
            presentacion_producto.setNombre_presentacion("Frasco");
            presentacion_producto.setClave_presentacion("en-01");
            presentacion_producto.setDescripcion_presentacion("--");
            pojo_producto.setPresentacion_producto(presentacion_producto);

            PojoUnidadMedida unidad_medida_prod = new PojoUnidadMedida();
            unidad_medida_prod.setId_unidad_medida(1);
            unidad_medida_prod.setNombre_unidad_medida("unidades");
            unidad_medida_prod.setClave_unidad_medida("n-01");
            unidad_medida_prod.setDescripcion_unidad_medida("--");
            pojo_producto.setUnidad_medida_producto(unidad_medida_prod);

            pojo_producto.setNombre_producto(contentTable.get(i).getName_item());
            float precio_unitario = contentTable.get(i).getCost_item() / contentTable.get(i).getQuantity_item();
            pojo_producto.setPrecio_producto("" + precio_unitario);
            pojo_producto.setCantidad_producto("" + contentTable.get(i).getQuantity_item());

            list_pojo_productos.add(pojo_producto);
        }

        lista_productos_usuario.setId_estimado_gasto((int) id_estimado_gasto);
        lista_productos_usuario.setId_super((int) id_supermercado);
        lista_productos_usuario.setFecha_lista(fecha_compra);
        lista_productos_usuario.setLista_productos(list_pojo_productos);

        service_caller.setListaProductos(lista_productos_usuario, this);
    }

    @Override
    public void listSended() {
        progress.dismiss();
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.xml_launch_form_new_item:
                goFragment(false, null);
                break;
            case R.id.xml_btn_fin_asistente_comp:

                if (initial_budget == user_budget) {
                    Toast toast1 = Toast.makeText(getContext(), "Debes comprar al menos un producto ;)", Toast.LENGTH_SHORT);
                    toast1.show();
                } else {

                    progress.show();

                    String s[] = datos_usuario.split(",");

                    float id_usuario = Float.parseFloat(s[0]);

                    /**
                     * Comienzo a hacer el empaquetado de el objeto presupuesto de compra
                     * para subirlo al servidor
                     */
                    PojoTitulo pojo_titulo = new PojoTitulo();
                    pojo_titulo.setId_titulo(1);

                    PojoUsuario pojo_usuario = new PojoUsuario();
                    pojo_usuario.setId_usuario((int) id_usuario);
                    pojo_usuario.setTitulo_usuario(pojo_titulo);

                    PojoCentroComercial centro_comercial = new PojoCentroComercial();
                    centro_comercial.setId_centro_comercial(2);

                    Time today = new Time(Time.getCurrentTimezone());
                    today.setToNow();
                    int monthDay = today.monthDay;
                    int month = today.month + 1;
                    int year = today.year;

                    fecha_compra = monthDay + "/" + month + "/" + year;

                    PojoPresupuestoCompra presupuesto_compra_usuario = new PojoPresupuestoCompra();
                    presupuesto_compra_usuario.setId_compra(0);
                    presupuesto_compra_usuario.setPojoCentroComercial(centro_comercial);
                    presupuesto_compra_usuario.setPojoUsuario(pojo_usuario);
                    presupuesto_compra_usuario.setPresupuesto_compra("" + initial_budget);
                    presupuesto_compra_usuario.setMonto_final_compra(user_budget);
                    presupuesto_compra_usuario.setFecha_compra(fecha_compra);

                    /**
                     * Termino de hacer el empaquetado del objeto y se lo envio al servidor
                     * por medio de Service_Caller que es un objeto de mi clase RestApi
                     * el cual se encarga de enviar y pedir cosas al servidor
                     */
                    service_caller.setPresupuestoCompra(presupuesto_compra_usuario, this);

                    break;
                }
        }
    }

    public void updateTable() {

        TextView mTextView;

        mTableRow = new TableRow(fragmentView.getContext());
        String[] strings_row = {"Nombre", "Marca", "Presentación", "Monto", ""};
        mTableRow = new TableRow(fragmentView.getContext());
        for (int i = 0; i < 5; i++) {
            mTextView = new TextView(fragmentView.getContext());
            mTextView.setGravity(Gravity.CENTER);
            mTextView.setText(strings_row[i]);
            mTextView.setPadding(10, 10, 10, 10);
            mTextView.setBackgroundResource(R.color.colorPrimaryDark);
            mTextView.setTextSize(20);
            mTextView.setTextColor(Color.WHITE);
            mTableRow.addView(mTextView);
        }
        jv_table_list_items.addView(mTableRow);

        /**
         * Pinto los elementos de la tabla con base en la lista
         * que se llama contentTable
         */
        for (int j = 0; j < contentTable.size(); j++) {

            String[] name_items_row = {
                    contentTable.get(j).getName_item(),
                    contentTable.get(j).getBrand_item(),
                    contentTable.get(j).getFiling_item(),
                    "" + contentTable.get(j).getCost_item()
            };

            mTableRow = new TableRow(fragmentView.getContext());
            mTableRow.setTag(j);

            for (int i = 0; i < 4; i++) {
                mTextView = new TextView(fragmentView.getContext());
                mTextView.setGravity(Gravity.CENTER);
                mTextView.setText(name_items_row[i]);
                mTextView.setPadding(10, 10, 10, 10);
                mTextView.setTextSize(20);
                mTextView.setTextColor(Color.BLACK);
                mTableRow.addView(mTextView);
            }
            delete_item = new Button(fragmentView.getContext());
            delete_item.setText("Borrar");
            delete_item.setTag("" + (j));

            delete_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int j = Integer.parseInt("" + view.getTag());

                    Log.i(TAG, "onClick: eliminaste " + contentTable.get(j).getName_item());

                    user_budget = user_budget + contentTable.get(j).getCost_item();
                    jv_txt_user_final_budget.setText("" + user_budget);

                    contentTable.remove(j);
                    jv_table_list_items.removeAllViews();
                    updateTable();
                }
            });

            mTableRow.addView(delete_item);

            mTableRow.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {

                    int j = Integer.parseInt("" + view.getTag());
                    Log.i(TAG, "onLongClick: seleciconaste " + j);
                    goFragment(true, contentTable.get(j));
                    contentTable.remove(j);

                    return true;
                }
            });

            jv_table_list_items.addView(mTableRow);


        }


    }

    public void goFragment(boolean flag, NewItemModel itemSet) {
        AgregarProductoFragment agregarProductoFragment = new AgregarProductoFragment();

        agregarProductoFragment.setAsistente_comp_interface(this);
        if (flag) {
            user_budget = user_budget + itemSet.getCost_item();
            agregarProductoFragment.updateItem(itemSet, flag);
        }
        agregarProductoFragment.setUser_budget(user_budget);

        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.mainContainer, agregarProductoFragment, "myFragmentTag")
                .addToBackStack("agregarProductoFragment")
                .commit();

    }

    public void setUser_budget(float user_budget) {
        this.user_budget = user_budget;
        initial_budget = user_budget;
    }

    public void setSupermercado_usuario(String supermercado_usuario) {
        this.supermercado_usuario = supermercado_usuario;
    }

    public void setDatos_usuario(String datos_usuario) {
        this.datos_usuario = datos_usuario;
    }
}