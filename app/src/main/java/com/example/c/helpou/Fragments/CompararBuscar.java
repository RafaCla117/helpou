package com.example.c.helpou.Fragments;

import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.c.helpou.Fragments.FragmentsInterfaces.ComparaBuscaInterface;
import com.example.c.helpou.Modelos.NewItemModel;
import com.example.c.helpou.Modelos.PojoGetListItems;
import com.example.c.helpou.R;
import com.example.c.helpou.Retrofit.RestApi;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class CompararBuscar extends Fragment implements View.OnClickListener, ComparaBuscaInterface {

    private static final String TAG = "TAG - CompararBuscar";
    private View fragmentView;
    private TableLayout jv_table_user_items;
    private TableRow mTableRow;
    private EditText jv_item_name_to_search;
    private Button search_button;
    private String item_name;
    private List<NewItemModel> lista_productos_encontrados = new ArrayList<>();
    private RestApi caller_service;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_comparar_buscar, container, false);

        search_button = fragmentView.findViewById(R.id.search_button_nombre_prod);
        jv_item_name_to_search = fragmentView.findViewById(R.id.xml_edtxt_item_name_to_search);
        jv_table_user_items = fragmentView.findViewById(R.id.xml_table_search_compare_items);


        search_button.setOnClickListener(this);

        return fragmentView;
    }

    @Override
    public void onClick(View view) {
        item_name = "" + jv_item_name_to_search.getText();

        if (validateString(item_name)) {
            lista_productos_encontrados.clear();
            jv_table_user_items.removeAllViews();
            searchItem(item_name);
        } else {
            Toast toast1 = Toast.makeText(getContext(), "Cadena no valida porfavor verifica  ;)", Toast.LENGTH_LONG);
            toast1.show();
        }

    }

    public void searchItem(String name) {
        Log.i(TAG, "searchItem: Buscao item: " + name);
        caller_service = new RestApi(getContext());

        PojoGetListItems pojo_get_list_items = new PojoGetListItems();
        pojo_get_list_items.setNombre_item(name);

        caller_service.getListItems(pojo_get_list_items, this);
    }

    public void updateTable() {

        TextView mTextView;
        String[] strings_row = {"Nombre", "Marca", "Supermercado", "Precio"};
        mTableRow = new TableRow(fragmentView.getContext());

        for (int i = 0; i < 4; i++) {
            mTextView = new TextView(fragmentView.getContext());
            mTextView.setGravity(Gravity.CENTER);
            mTextView.setText(strings_row[i]);
            mTextView.setPadding(10, 10, 10, 10);
            mTextView.setBackgroundResource(R.color.colorPrimaryDark);
            mTextView.setTextSize(20);
            mTextView.setTextColor(Color.WHITE);
            mTableRow.addView(mTextView);
        }
        jv_table_user_items.addView(mTableRow);

        for (int j = 0; j < lista_productos_encontrados.size(); j++) {

            String[] strings_items_row = {
                    lista_productos_encontrados.get(j).getName_item(),
                    lista_productos_encontrados.get(j).getFiling_item(),
                    lista_productos_encontrados.get(j).getBrand_item(),
                    "" + lista_productos_encontrados.get(j).getCost_item()
            };

            mTableRow = new TableRow(fragmentView.getContext());
            for (int i = 0; i < 4; i++) {
                mTextView = new TextView(fragmentView.getContext());
                mTextView.setGravity(Gravity.CENTER);
                mTextView.setText(strings_items_row[i]);
                mTextView.setPadding(10, 10, 10, 10);
                mTextView.setBackgroundResource(R.color.colorWhite);
                mTextView.setTextSize(20);
                mTextView.setTextColor(Color.BLACK);
                mTableRow.addView(mTextView);
            }

            mTableRow.setTag(j);

            jv_table_user_items.addView(mTableRow);
        }


    }

    public boolean validateString(String s) {

        if (s == null || s.trim().isEmpty()) {
            Log.i(TAG, "validateString: Incorrect format of string");
            return false;
        }

        Pattern p = Pattern.compile("^[\\p{L} .-]+$");
        Matcher m = p.matcher(s);

        boolean b = m.find();

        /*
        if (b == true)
            Log.i(TAG, "validateString: there is a valid string");
        else
            Log.i(TAG, "validateString: there isn't a valid sting");
        */

        return b;
    }

    @Override
    public void setListItems(String string_content_table) {

        string_content_table = string_content_table.replace(" ", "");


        ArrayList<String> stringArray = new ArrayList<>();

        JSONArray jsonArray;

        try {
            jsonArray = new JSONArray(string_content_table);

            for (int i = 0; i < jsonArray.length(); i++) {
                stringArray.add(jsonArray.getString(i));
                Log.i(TAG, "setTableContent: ------------> " + stringArray.get(i));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        String aux_1;

        for (int i = 0; i < stringArray.size(); i++) {
            aux_1 = stringArray.get(i);
            aux_1 = aux_1.replace("[", "");
            aux_1 = aux_1.replace("]", "");
            aux_1 = aux_1.replace("\"", "");
            String[] array_aux = aux_1.split(",");

            NewItemModel item_obtained = new NewItemModel();

            item_obtained.setName_item(array_aux[0]);
            item_obtained.setFiling_item(array_aux[1]);
            item_obtained.setCost_item(Float.parseFloat(array_aux[2]));
            item_obtained.setBrand_item(array_aux[3]);

            lista_productos_encontrados.add(item_obtained);

        }
        updateTable();
    }
}
