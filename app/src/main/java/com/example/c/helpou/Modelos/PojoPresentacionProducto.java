package com.example.c.helpou.Modelos;

import com.google.gson.annotations.SerializedName;

/**
 * Created by C on 06/11/2017.
 */

public class PojoPresentacionProducto {

    @SerializedName("idTipoPresentacion")
    int id_tipo_presentacion;
    @SerializedName("nombre")
    String nombre_presentacion;
    @SerializedName("clave")
    String clave_presentacion;
    @SerializedName("descripcion")
    String descripcion_presentacion;

    public int getId_tipo_presentacion() {
        return id_tipo_presentacion;
    }

    public void setId_tipo_presentacion(int id_tipo_presentacion) {
        this.id_tipo_presentacion = id_tipo_presentacion;
    }

    public String getNombre_presentacion() {
        return nombre_presentacion;
    }

    public void setNombre_presentacion(String nombre_presentacion) {
        this.nombre_presentacion = nombre_presentacion;
    }

    public String getClave_presentacion() {
        return clave_presentacion;
    }

    public void setClave_presentacion(String clave_presentacion) {
        this.clave_presentacion = clave_presentacion;
    }

    public String getDescripcion_presentacion() {
        return descripcion_presentacion;
    }

    public void setDescripcion_presentacion(String descripcion_presentacion) {
        this.descripcion_presentacion = descripcion_presentacion;
    }

    @Override
    public String toString() {
        return "PojoPresentacionProducto{" +
                "id_tipo_presentacion='" + id_tipo_presentacion + '\'' +
                ", nombre_presentacion='" + nombre_presentacion + '\'' +
                ", clave_presentacion='" + clave_presentacion + '\'' +
                ", descripcion_presentacion='" + descripcion_presentacion + '\'' +
                '}';
    }
}
