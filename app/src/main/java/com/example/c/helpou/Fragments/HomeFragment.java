package com.example.c.helpou.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.c.helpou.Fragments.FragmentsInterfaces.HomeFragmentInterface;
import com.example.c.helpou.Modelos.PojoRequestTitle;
import com.example.c.helpou.R;
import com.example.c.helpou.Retrofit.RestApi;


public class HomeFragment extends Fragment implements HomeFragmentInterface {

    private String TAG = "TAG";
    private View fragment_view;
    private ProgressBar user_title_progress_bar;
    private String name_usuario, app_usuario, titulo_usuario, datos_usuario;
    private float id_usuario, genero_usuario;
    private TextView jv_txtv_titulo_usuario, jv_txtv_nombre_usuario;
    private RestApi caller_service;

    private String man_titles[] = {"Iniciado", "Avanzado", "Profecional", "Cazador de Ofertas", "Ofertista", "Comprador Maestro"};
    private String woman_titles[] = {"Iniciada", "Avanzada", "Profecional", "Cazadora de Ofertas", "Ofertista", "Compradora Maestra"};

    private int user_title_progress = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragment_view = inflater.inflate(R.layout.fragment_home, container, false);

        user_title_progress_bar = (ProgressBar) fragment_view.findViewById(R.id.progress_bar_title_user);
        jv_txtv_nombre_usuario = (TextView) fragment_view.findViewById(R.id.xml_txtv_nombre_usuario);
        jv_txtv_titulo_usuario = (TextView) fragment_view.findViewById(R.id.xml_txtv_titulo_usuario);

        caller_service = new RestApi(getContext());


        String s[] = datos_usuario.split(",");

        id_usuario = Float.parseFloat(s[0]);
        name_usuario = s[1];
        app_usuario = s[2];
        titulo_usuario = s[4];
        genero_usuario = Float.parseFloat(s[5]);

        PojoRequestTitle pojoRequestTitle = new PojoRequestTitle();
        pojoRequestTitle.setId_usuario("" + (int) id_usuario);

        Log.i(TAG, "onCreateView() id del usuario a consultar: " + (int) id_usuario);

        caller_service.getTitleUser(pojoRequestTitle, this);

        jv_txtv_nombre_usuario.setText("Hola " + name_usuario);
        if (genero_usuario == 1) {
            jv_txtv_titulo_usuario.setText("el " + titulo_usuario);
        } else {
            jv_txtv_titulo_usuario.setText("la " + titulo_usuario);
        }

        return fragment_view;
    }

    public void setDatos_usuario(String datos_usuario) {
        this.datos_usuario = datos_usuario;
    }

    @Override
    public void paintProgress(int no_compras) {
        user_title_progress_bar.setProgress(no_compras * 5);
    }
}
