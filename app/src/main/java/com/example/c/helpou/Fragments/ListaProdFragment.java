package com.example.c.helpou.Fragments;

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.c.helpou.Fragments.FragmentsInterfaces.ListaProdInterface;
import com.example.c.helpou.Modelos.NewItemModel;
import com.example.c.helpou.Modelos.PojoIdUsuario;
import com.example.c.helpou.Modelos.PojoRequestItemsByDate;
import com.example.c.helpou.R;
import com.example.c.helpou.Retrofit.RestApi;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class ListaProdFragment extends Fragment implements ListaProdInterface {

    private static final String TAG = "TAG - ListaProdFragment";
    private View fragmentView;
    private Spinner spinner;
    private TableLayout jv_table_user_items;
    private TableRow mTableRow;
    private TextView mTextView;
    private String datos_user;
    private RestApi caller_service;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_lista_prod, container, false);
        jv_table_user_items = fragmentView.findViewById(R.id.xml_table_user_items);
        spinner = fragmentView.findViewById(R.id.spinner);

        caller_service = new RestApi(getContext());

        Log.i(TAG, " onCreateView() salida de json: " + datos_user);

        ArrayList<String> stringArray = new ArrayList<>();

        JSONArray jsonArray;

        try {
            jsonArray = new JSONArray(datos_user);

            for (int i = 0; i < jsonArray.length(); i++) {
                stringArray.add(jsonArray.getString(i));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        PojoIdUsuario pojo_id_us = new PojoIdUsuario();
        pojo_id_us.setId_usuario((int) Float.parseFloat(stringArray.get(0)));


        caller_service.getListaFechasListasP(pojo_id_us, this);

        return fragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        caller_service = new RestApi(getContext());
    }

    public void setDatos_user(String datos_user) {
        this.datos_user = datos_user;
    }

    public void initListFechas(final String[] valores) {
        spinner.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, valores));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                Log.i(TAG, "onItemSelected: seleccionaste: " + valores[position]);

                jv_table_user_items.removeAllViews();

                ArrayList<String> stringArray = new ArrayList<>();

                JSONArray jsonArray;

                try {
                    jsonArray = new JSONArray(datos_user);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        stringArray.add(jsonArray.getString(i));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                PojoRequestItemsByDate pojo_id_us = new PojoRequestItemsByDate();
                pojo_id_us.setId_usuario((int) Float.parseFloat(stringArray.get(0)));
                pojo_id_us.setFecha_compra(valores[position]);

                caller_service.getItemsByDate(pojo_id_us, ListaProdFragment.this);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // vacio
            }
        });
    }

    @Override
    public void setListFechas(String list_fectas) {
        list_fectas = list_fectas.replace("[", "");
        list_fectas = list_fectas.replace("]", "");
        list_fectas = list_fectas.replace(" ", "");
        Log.i(TAG, "setListFechas: lista de fechas obtenidas: " + list_fectas);
        String[] aux_array = list_fectas.split(",");

        initListFechas(aux_array);
    }

    @Override
    public void setTableContent(String string_content_table) {

        ArrayList<NewItemModel> contentTable = new ArrayList<>();

        string_content_table = string_content_table.replace(" ", "");


        ArrayList<String> stringArray = new ArrayList<>();

        JSONArray jsonArray;

        try {
            jsonArray = new JSONArray(string_content_table);

            for (int i = 0; i < jsonArray.length(); i++) {
                stringArray.add(jsonArray.getString(i));
                Log.i(TAG, "setTableContent: ------------> " + stringArray.get(i));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        String aux_1;

        for (int i = 0; i < stringArray.size(); i++) {
            aux_1 = stringArray.get(i);
            aux_1 = aux_1.replace("[", "");
            aux_1 = aux_1.replace("]", "");
            aux_1 = aux_1.replace("\"","");
            String[] array_aux = aux_1.split(",");

            NewItemModel item_obtained = new NewItemModel();

            item_obtained.setName_item(array_aux[0]);
            item_obtained.setCost_item(Float.parseFloat(array_aux[1]));
            item_obtained.setBrand_item(array_aux[3]);

            contentTable.add(item_obtained);
        }

        mTableRow = new TableRow(fragmentView.getContext());
        String[] strings_row = {"Nombre", "Marca", "Precio"};

        for (int i = 0; i < strings_row.length; i++) {
            mTextView = new TextView(fragmentView.getContext());
            mTextView.setGravity(Gravity.CENTER);
            mTextView.setText(strings_row[i]);
            mTextView.setPadding(10, 10, 10, 10);
            mTextView.setBackgroundResource(R.color.colorPrimaryDark);
            mTextView.setTextSize(20);
            mTextView.setTextColor(Color.WHITE);
            mTableRow.addView(mTextView);
        }
        jv_table_user_items.addView(mTableRow);


        for (int j = 0; j < contentTable.size(); j++) {

            String[] name_items_row = {
                    contentTable.get(j).getName_item(),
                    contentTable.get(j).getBrand_item(),
                    "" + contentTable.get(j).getCost_item()
            };

            mTableRow = new TableRow(fragmentView.getContext());
            mTableRow.setTag(j);

            for (int i = 0; i < 3; i++) {
                mTextView = new TextView(fragmentView.getContext());
                mTextView.setGravity(Gravity.CENTER);
                mTextView.setText(name_items_row[i]);
                mTextView.setPadding(10, 10, 10, 10);
                mTextView.setTextSize(20);
                mTextView.setTextColor(Color.BLACK);
                mTableRow.addView(mTextView);
            }


            jv_table_user_items.addView(mTableRow);
        }


    }
}
