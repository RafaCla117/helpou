package com.example.c.helpou.Modelos;

import com.google.gson.annotations.SerializedName;

/**
 * Created by C on 08/10/2017.
 */

public class PojoTipoPerfil {

    @SerializedName("idTipoPerfil")
    int id_tipo_perfil;
    @SerializedName("nombre")
    String nombre_perfil;
    @SerializedName("clave")
    String clase_perfil;
    @SerializedName("descripcion")
    String descripcion_perfil;


    public int getId_tipo_perfil() {
        return id_tipo_perfil;
    }

    public void setId_tipo_perfil(int id_tipo_perfil) {
        this.id_tipo_perfil = id_tipo_perfil;
    }

    public String getNombre_perfil() {
        return nombre_perfil;
    }

    public void setNombre_perfil(String nombre_perfil) {
        this.nombre_perfil = nombre_perfil;
    }

    public String getClase_perfil() {
        return clase_perfil;
    }

    public void setClase_perfil(String clase_perfil) {
        this.clase_perfil = clase_perfil;
    }

    public String getDescripcion_perfil() {
        return descripcion_perfil;
    }

    public void setDescripcion_perfil(String descripcion_perfil) {
        this.descripcion_perfil = descripcion_perfil;
    }

    @Override
    public String toString() {
        return "PojoTipoPerfil{" +
                "id_tipo_perfil=" + id_tipo_perfil +
                ", nombre_perfil='" + nombre_perfil + '\'' +
                ", clase_perfil='" + clase_perfil + '\'' +
                ", descripcion_perfil='" + descripcion_perfil + '\'' +
                '}';
    }
}
