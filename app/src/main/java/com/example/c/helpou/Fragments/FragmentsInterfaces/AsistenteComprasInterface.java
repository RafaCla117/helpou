package com.example.c.helpou.Fragments.FragmentsInterfaces;

import com.example.c.helpou.Modelos.NewItemModel;

/**
 * Created by C on 18/10/2017.
 */

public interface AsistenteComprasInterface {

    /**
     * Esta metodo lo llamamos para agregar un
     * nuevo item a nuestra tabla que se muestra
     * en la vista de asistente de compras
     * @param new_item es el item que agregamos a la tabla
     */
    void addNewItem(NewItemModel new_item);

    /**
     * Este metodo lo mandamos a llamar cuando insertamos una
     * lista con presupuesto a nuestr servidor
     * @param id_compra es el id de la lista que se inserto
     *                  en nustro servidor
     */
    void setNewListItems(String id_compra);


    void listSended();

}
