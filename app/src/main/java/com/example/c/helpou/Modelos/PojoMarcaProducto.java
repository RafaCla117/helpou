package com.example.c.helpou.Modelos;

import com.google.gson.annotations.SerializedName;

/**
 * Created by C on 06/11/2017.
 */

public class PojoMarcaProducto {

    @SerializedName("idMarca")
    int id_marca_producto;
    @SerializedName("nombre")
    String nombre_marca;
    @SerializedName("clave")
    String clave_producto;
    @SerializedName("descripcion")
    String descripcion_poducto;

    public int getId_marca_producto() {
        return id_marca_producto;
    }

    public void setId_marca_producto(int id_marca_producto) {
        this.id_marca_producto = id_marca_producto;
    }

    public String getNombre_marca() {
        return nombre_marca;
    }

    public void setNombre_marca(String nombre_marca) {
        this.nombre_marca = nombre_marca;
    }

    public String getClave_producto() {
        return clave_producto;
    }

    public void setClave_producto(String clave_producto) {
        this.clave_producto = clave_producto;
    }

    public String getDescripcion_poducto() {
        return descripcion_poducto;
    }

    public void setDescripcion_poducto(String descripcion_poducto) {
        this.descripcion_poducto = descripcion_poducto;
    }

    @Override
    public String toString() {
        return "PojoMarcaProducto{" +
                "id_marca_producto='" + id_marca_producto + '\'' +
                ", nombre_marca='" + nombre_marca + '\'' +
                ", clave_producto='" + clave_producto + '\'' +
                ", descripcion_poducto='" + descripcion_poducto + '\'' +
                '}';
    }
}
