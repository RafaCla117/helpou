package com.example.c.helpou.Modelos;

import com.google.gson.annotations.SerializedName;

/**
 * Created by C on 05/11/2017.
 */

public class PojoPresupuestoCompra {

    @SerializedName("idCompra")
    int id_compra;
    @SerializedName("centroComercial")
    PojoCentroComercial pojoCentroComercial;
    @SerializedName("usuario")
    PojoUsuario pojoUsuario;
    @SerializedName("montoFinalDeCompra")
    float monto_final_compra;
    @SerializedName("presupuesto")
    String presupuesto_compra;
    @SerializedName("fechaCompra")
    String fecha_compra;

    public int getId_compra() {
        return id_compra;
    }

    public void setId_compra(int id_compra) {
        this.id_compra = id_compra;
    }

    public PojoCentroComercial getPojoCentroComercial() {
        return pojoCentroComercial;
    }

    public void setPojoCentroComercial(PojoCentroComercial pojoCentroComercial) {
        this.pojoCentroComercial = pojoCentroComercial;
    }

    public PojoUsuario getPojoUsuario() {
        return pojoUsuario;
    }

    public void setPojoUsuario(PojoUsuario pojoUsuario) {
        this.pojoUsuario = pojoUsuario;
    }

    public float getMonto_final_compra() {
        return monto_final_compra;
    }

    public void setMonto_final_compra(float monto_final_compra) {
        this.monto_final_compra = monto_final_compra;
    }

    public String getPresupuesto_compra() {
        return presupuesto_compra;
    }

    public void setPresupuesto_compra(String presupuesto_compra) {
        this.presupuesto_compra = presupuesto_compra;
    }

    public String getFecha_compra() {
        return fecha_compra;
    }

    public void setFecha_compra(String fecha_compra) {
        this.fecha_compra = fecha_compra;
    }

    @Override
    public String toString() {
        return "PojoPresupuestoCompra{" +
                "id_compra=" + id_compra +
                ", pojoCentroComercial=" + pojoCentroComercial +
                ", pojoUsuario=" + pojoUsuario +
                ", monto_final_compra=" + monto_final_compra +
                ", presupuesto_compra='" + presupuesto_compra + '\'' +
                ", fecha_compra='" + fecha_compra + '\'' +
                '}';
    }

}
