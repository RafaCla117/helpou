package com.example.c.helpou.Modelos;

import com.google.gson.annotations.SerializedName;

/**
 * Created by C on 06/11/2017.
 */

public class PojoUnidadMedida {

    @SerializedName("idUnidadDeMedida")
    int id_unidad_medida;
    @SerializedName("nombre")
    String nombre_unidad_medida;
    @SerializedName("clave")
    String clave_unidad_medida;
    @SerializedName("descripcion")
    String descripcion_unidad_medida;

    public int getId_unidad_medida() {
        return id_unidad_medida;
    }

    public void setId_unidad_medida(int id_unidad_medida) {
        this.id_unidad_medida = id_unidad_medida;
    }

    public String getNombre_unidad_medida() {
        return nombre_unidad_medida;
    }

    public void setNombre_unidad_medida(String nombre_unidad_medida) {
        this.nombre_unidad_medida = nombre_unidad_medida;
    }

    public String getClave_unidad_medida() {
        return clave_unidad_medida;
    }

    public void setClave_unidad_medida(String clave_unidad_medida) {
        this.clave_unidad_medida = clave_unidad_medida;
    }

    public String getDescripcion_unidad_medida() {
        return descripcion_unidad_medida;
    }

    public void setDescripcion_unidad_medida(String descripcion_unidad_medida) {
        this.descripcion_unidad_medida = descripcion_unidad_medida;
    }

    @Override
    public String toString() {
        return "PojoUnidadMedida{" +
                "id_unidad_medida=" + id_unidad_medida +
                ", nombre_unidad_medida='" + nombre_unidad_medida + '\'' +
                ", clave_unidad_medida='" + clave_unidad_medida + '\'' +
                ", descripcion_unidad_medida='" + descripcion_unidad_medida + '\'' +
                '}';
    }
}
