package com.example.c.helpou.Modelos;

import com.google.gson.annotations.SerializedName;

/**
 * Created by C on 04/11/2017.
 */

public class PojoRecuperarContraseña {

    @SerializedName("correo")
    String correo_usuario;

    public String getCorreo_usuario() {
        return correo_usuario;
    }

    public void setCorreo_usuario(String correo_usuario) {
        this.correo_usuario = correo_usuario;
    }

    @Override
    public String toString() {
        return "PojoRecuperarContraseña{" +
                "correo_usuario='" + correo_usuario + '\'' +
                '}';
    }
}
