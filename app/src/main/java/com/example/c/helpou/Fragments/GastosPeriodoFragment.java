package com.example.c.helpou.Fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.c.helpou.Fragments.FragmentsInterfaces.GastosPerdioInterface;
import com.example.c.helpou.Modelos.PojoIdUsuario;
import com.example.c.helpou.R;
import com.example.c.helpou.Retrofit.RestApi;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class GastosPeriodoFragment extends Fragment implements GastosPerdioInterface {

    private static final String TAG = "TAG - GastosPeriodoFragment";
    private PieChart pie_chart_gastos_tiempo;
    private View fragment_view;
    private String datos_usuario;
    private RestApi service_caller;
    private List<String> lista_fechas = new ArrayList<>();
    private List<Float> lista_gastos = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragment_view = inflater.inflate(R.layout.fragment_gastos_periodo, container, false);
        pie_chart_gastos_tiempo = fragment_view.findViewById(R.id.xml_pie_chart_gastos_periodo_tiempo);


        service_caller = new RestApi(getContext());

        callData();

        return fragment_view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        service_caller = new RestApi(getContext());
        callData();
    }

    @Override
    public void onResume() {
        super.onResume();
        service_caller = new RestApi(getContext());
        callData();
    }

    @Override
    public void onStart() {
        super.onStart();
        service_caller = new RestApi(getContext());
        callData();
    }

    public void callData() {

        ArrayList<String> stringArray = new ArrayList<>();

        JSONArray jsonArray;

        try {
            jsonArray = new JSONArray(datos_usuario);

            for (int i = 0; i < jsonArray.length(); i++) {
                stringArray.add(jsonArray.getString(i));
                Log.i(TAG, "callData: --------------------" + jsonArray.getString(i));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        PojoIdUsuario pojo_id_usuario = new PojoIdUsuario();
        pojo_id_usuario.setId_usuario((int) Float.parseFloat(stringArray.get(0)));

        service_caller.getGastosPeriodoTiempo(pojo_id_usuario, this);

    }

    public void setDatos_usuario(String datos_usuario) {
        this.datos_usuario = datos_usuario;
    }

    public void paintGraph() {

        //Propiedades de la grafica
        pie_chart_gastos_tiempo.setUsePercentValues(false);
        pie_chart_gastos_tiempo.getDescription().setEnabled(false);
        pie_chart_gastos_tiempo.setExtraOffsets(5, 10, 5, 5);
        pie_chart_gastos_tiempo.setHoleRadius(0.0f);
        pie_chart_gastos_tiempo.setDragDecelerationFrictionCoef(0.95f);
        pie_chart_gastos_tiempo.animateY(1000, Easing.EasingOption.EaseInOutCubic);

        ArrayList<PieEntry> yValues = new ArrayList<>();

        for (int i = 0; i < lista_fechas.size(); i++) {
            yValues.add(new PieEntry(lista_gastos.get(i), "Dia: " + lista_fechas.get(i)));
        }

        PieDataSet data_set = new PieDataSet(yValues, "");
        data_set.setSliceSpace(3f);
        data_set.setSliceSpace(5f);
        data_set.setColors(ColorTemplate.JOYFUL_COLORS);

        PieData pie_data = new PieData(data_set);
        pie_data.setValueTextSize(10f);
        pie_data.setValueTextColor(Color.BLACK);

        pie_chart_gastos_tiempo.setData(pie_data);

        lista_gastos.clear();
        lista_fechas.clear();

    }

    @Override
    public void paintData(String data) {
        data = data.replace("[", "");
        data = data.replace("]", "");
        data = data.replace(" ", "");
        Log.i(TAG, "paintData: datos recibidos: " + data);

        String[] my_array = data.split(",");

        for (int i = 0; i < my_array.length; i++) {
            if (i % 2 == 0) {
                lista_gastos.add(Float.parseFloat(my_array[i]));
            } else {
                lista_fechas.add(my_array[i]);
            }
        }

        paintGraph();

    }
}
