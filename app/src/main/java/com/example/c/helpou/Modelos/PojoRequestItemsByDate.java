package com.example.c.helpou.Modelos;

import com.google.gson.annotations.SerializedName;

/**
 * Created by C on 07/11/2017.
 */

public class PojoRequestItemsByDate {

    @SerializedName("idUsuario")
    int id_usuario;
    @SerializedName("fechaNacimiento")
    String fecha_compra;

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getFecha_compra() {
        return fecha_compra;
    }

    public void setFecha_compra(String fecha_compra) {
        this.fecha_compra = fecha_compra;
    }

    @Override
    public String toString() {
        return "PojoRequestItemsByDate{" +
                "id_usuario=" + id_usuario +
                ", fecha_compra='" + fecha_compra + '\'' +
                '}';
    }
}
