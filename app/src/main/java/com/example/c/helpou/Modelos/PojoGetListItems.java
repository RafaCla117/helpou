package com.example.c.helpou.Modelos;

import com.google.gson.annotations.SerializedName;

/**
 * Created by C on 07/11/2017.
 */

public class PojoGetListItems {

    @SerializedName("nombre")
    String nombre_item;

    public String getNombre_item() {
        return nombre_item;
    }

    public void setNombre_item(String nombre_item) {
        this.nombre_item = nombre_item;
    }

    @Override
    public String toString() {
        return "PojoGetListItems{" +
                "nombre_item='" + nombre_item + '\'' +
                '}';
    }
}
