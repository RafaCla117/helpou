package com.example.c.helpou.Views;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.example.c.helpou.Fragments.AsistenteCompras;
import com.example.c.helpou.Fragments.CompararBuscar;
import com.example.c.helpou.Fragments.HistoricosFragment;
import com.example.c.helpou.Fragments.HomeFragment;
import com.example.c.helpou.R;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MenuApp extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private String TAG = "TAG";
    PopupWindow popBudgetWindow;
    View inflated_window_budget_view;
    String datos_usuario, supermercado_usuario = "";
    Button jv_btn_walmart, jv_btn_chedraui;
    double longitude_GPS, latitud_GPS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_app);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        datos_usuario = getIntent().getStringExtra("user_data");

        shoHomeFragment();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        shoHomeFragment();
    }

    @Override
    protected void onResume() {
        super.onResume();
        shoHomeFragment();
    }

    @Override
    protected void onStart() {
        super.onStart();
        shoHomeFragment();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.menuOpc1) {

            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            inflated_window_budget_view = layoutInflater.inflate(R.layout.pop_window_set_budget, null, false);

            jv_btn_walmart = inflated_window_budget_view.findViewById(R.id.xml_btn_select_walmat);
            jv_btn_chedraui = inflated_window_budget_view.findViewById(R.id.xml_btn_select_chedraui);

            jv_btn_walmart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    supermercado_usuario = "Walmart";
                    jv_btn_walmart.setEnabled(false);
                    jv_btn_chedraui.setEnabled(true);
                }
            });
            jv_btn_chedraui.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    supermercado_usuario = "Chedraui";
                    jv_btn_walmart.setEnabled(true);
                    jv_btn_chedraui.setEnabled(false);
                }
            });


            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            popBudgetWindow = new PopupWindow(inflated_window_budget_view, size.x - 100, size.y - 550, true);
            popBudgetWindow.setFocusable(true);
            popBudgetWindow.setBackgroundDrawable(getResources().getDrawable(R.drawable.popup_window_color));
            popBudgetWindow.showAtLocation(findViewById(R.id.main_containert), Gravity.CENTER, 0, 50);  // 0 - X postion and 150 - Y position

        } else if (id == R.id.menuOpc2) {
            HistoricosFragment historicosFragment = new HistoricosFragment();
            historicosFragment.setDatos_user(datos_usuario);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.mainContainer, historicosFragment)
                    .addToBackStack("fHistoricosFragment")
                    .commit();
        } else if (id == R.id.menuOpc3) {
            CompararBuscar compararBuscar = new CompararBuscar();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.mainContainer, compararBuscar)
                    .addToBackStack("fHistoricosFragment")
                    .commit();
        } else if (id == R.id.menuOpc4) {
            Intent mIntent = new Intent(this, MapsActivity.class);
            startActivity(mIntent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void launchNewFragment(Fragment newFragment, String flagBackStack) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.mainContainer, newFragment)
                .addToBackStack(flagBackStack)
                .commit();
    }

    public void launchFragmmentAsistenteCompras(View v) {
        EditText jvt_ing_budget = inflated_window_budget_view.findViewById(R.id.xml_edtxt_ing_budget);
        String str_user_budget = "" + jvt_ing_budget.getText();

        //getLocation();

        if (!str_user_budget.equals("")) {

            if (!supermercado_usuario.equals("")) {
                float user_budget = Float.parseFloat(str_user_budget);

                if (user_budget >= 100 && user_budget <= 5000) {
                    popBudgetWindow.dismiss();
                    AsistenteCompras asistenteCompras = new AsistenteCompras();
                    asistenteCompras.setUser_budget(user_budget);
                    asistenteCompras.setDatos_usuario(datos_usuario);
                    asistenteCompras.setSupermercado_usuario(supermercado_usuario);
                    supermercado_usuario = "";
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.mainContainer, asistenteCompras)
                            .addToBackStack("fHistoricosFragment")
                            .commit();
                } else {
                    Toast.makeText(getApplication(), "Debes ingresar un presupuesto mayor a 100 MXN y menor que 5,000 MXN ;)", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getApplication(), "Debes seleccionar un supermercado para continuar ;)", Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(getApplication(), "Debes ingresar un presupuesto válido para continuar ;)", Toast.LENGTH_SHORT).show();
        }

    }

    public void exit(View v) {
        finish();
    }

    public void shoHomeFragment() {
        HomeFragment home_fragment = new HomeFragment();
        home_fragment.setDatos_usuario(datos_usuario);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.mainContainer, home_fragment)
                .commit();
    }

}