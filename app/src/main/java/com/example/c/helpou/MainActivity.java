package com.example.c.helpou;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.example.c.helpou.Fragments.FragmentsInterfaces.MainActivityInterface;
import com.example.c.helpou.Modelos.PojoRecuperarContraseña;
import com.example.c.helpou.Modelos.PojoUsuarioSignIn;
import com.example.c.helpou.Retrofit.RestApi;
import com.example.c.helpou.Views.FormSignIn;
import com.example.c.helpou.Views.MenuApp;

import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MainActivity extends AppCompatActivity implements MainActivityInterface {

    private String TAG = "TAG";
    private static final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[_-]).{6,20})";
    Session session;
    EditText jvemail_forgotPass, jvet_email_user, jvet_pass_user;
    String emailToSendPass;
    String email_user, password_user, password_recovered;
    ProgressDialog progress;
    RestApi rest_api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progress = new ProgressDialog(this);
        rest_api = new RestApi(this);
        jvet_email_user = (EditText) findViewById(R.id.xml_email_sign_user);
        jvet_pass_user = (EditText) findViewById(R.id.xml_pass_sign_user);
    }

    @Override
    public void ValidUserDone(String user_data) {
        jvet_email_user.setText("");
        jvet_pass_user.setText("");
        Intent intent = new Intent(this, MenuApp.class);
        intent.putExtra("user_data", user_data);
        startActivity(intent);
    }

    @Override
    public void sendEmailForgotPass(String pass_user) {

        emailToSendPass = email_user;
        password_recovered = pass_user;

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        session = Session.getDefaultInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("suport.help.ou@gmail.com", "trabajo_terminal_2");
            }
        });


        new RetreiveFeedTask().execute();

    }

    @Override
    public void exceptionEmal() {
        progress.dismiss();
    }

    public void launchFormSinIn(View v1) {
        Intent mIntent = new Intent(this, FormSignIn.class);
        startActivity(mIntent);
    }

    public void launchMenu(View v2) {
        boolean flag1 = false, flag2 = false;

        email_user = "" + jvet_email_user.getText();
        password_user = "" + jvet_pass_user.getText();

        flag1 = validateEmail(email_user);

        if (!flag1) {
            Toast toast1 = Toast.makeText(getApplicationContext(), "E-mail no valido porfavor verifica  ;)", Toast.LENGTH_SHORT);
            toast1.show();
            flag2 = validatePassword(password_user);
            Log.i(TAG, "launchMenu: ------------------------------------------");
            Log.i(TAG, "launchMenu password: " + password_user);
        } else if (flag2) {
            Toast toast1 = Toast.makeText(getApplicationContext(), "Contraseña no valida porfavor verifica  ;)", Toast.LENGTH_SHORT);
            toast1.show();
        } else {
            PojoUsuarioSignIn pojo_usuario_sign = new PojoUsuarioSignIn();

            pojo_usuario_sign.setCorreo_user(email_user);
            pojo_usuario_sign.setPwd(password_user);

            rest_api.autenticarUsuario(pojo_usuario_sign, this);
        }

    }

    /**
     * Con esta funcion valido si hay caracteres especiales
     * dentro de la contraseña y si cumple con las condiciones
     * para una contraseña
     *
     * @param password la contraseña inspeccionar
     * @return false si tiene uncaracter especial y true si no tiene
     */
    public boolean validatePassword(String password) {

        if (password == null || password.trim().isEmpty()) {
            Log.i(TAG, "getSpecialCharacterCount: Incorrect format of string");
            return false;
        }

        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password);

        if (matcher.matches()) {
            Log.i(TAG, "validatePassword contraseña valida :) ");
            return true;
        } else {
            Log.i(TAG, "validatePassword: Contraseña no valida");
            return false;
        }

    }

    public void launchForgotPass(View v3) {
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View inflatedView = layoutInflater.inflate(R.layout.activity_forgot_pass, null, false);

        jvemail_forgotPass = inflatedView.findViewById(R.id.xmlEdTxtEmailForgotPass);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        PopupWindow popWindow = new PopupWindow(inflatedView, size.x - 20, size.y - 200, true);

        popWindow.setFocusable(true);
        popWindow.setBackgroundDrawable(getResources().getDrawable(R.drawable.popup_window_color));
        popWindow.showAtLocation(v3, Gravity.BOTTOM, 0, 50);  // 0 - X postion and 150 - Y position
        //popWindow.setAnimationStyle(R.anim.animation); // call this before showing the popup

    }

    /**
     * Validate given email with regular expression.
     *
     * @param email email for validation
     * @return true valid email, otherwise false
     */
    public static boolean validateEmail(String email) {

        // Compiles the given regular expression into a pattern.
        Pattern pattern = Pattern.compile(PATTERN_EMAIL);

        // Match the given input against this pattern
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }

    public boolean validateConsultEmail(View v) {

        email_user = "" + jvemail_forgotPass.getText();

        // Compiles the given regular expression into a pattern.
        Pattern pattern = Pattern.compile(PATTERN_EMAIL);

        // Match the given input against this pattern
        Matcher matcher = pattern.matcher(email_user);

        if (matcher.matches()) {

            PojoRecuperarContraseña objeto_correo_usuario = new PojoRecuperarContraseña();
            objeto_correo_usuario.setCorreo_usuario(email_user);

            progress = new ProgressDialog(this);
            progress.setMessage("Enviando E-mail, por favor espere...");
            progress.show();

            rest_api.getPassword(objeto_correo_usuario, this);

        } else {
            Toast toast1 = Toast.makeText(getApplicationContext(), "E-mail no valido porfavor verifica ;)", Toast.LENGTH_SHORT);
            toast1.setGravity(Gravity.CENTER, 50, 100);
            toast1.show();
            return false;
        }

        return true;
    }

    class RetreiveFeedTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            Log.i(TAG, "doInBackground() enviando e-mail...");

            Log.i(TAG, "doInBackground: e-mail al que voy a enviar " + emailToSendPass);
            Log.i(TAG, "doInBackground: contraseña enviada " + password_recovered);


            try {
                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress("testfrom354@gmail.com"));
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailToSendPass)); //Email al que enviaremos el correo
                message.setSubject("Recuperacion de contraseña");//Titulo del mensaje
                message.setContent("Hola Usuario HelpOu esta es tu contraseña: " + password_recovered, "text/html; charset=utf-8");//Mensaje
                Transport.send(message);
            } catch (MessagingException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progress.dismiss();
            jvemail_forgotPass.setText("");
            Toast.makeText(getApplicationContext(), "Contraseña enviada cheka tu e-mail ;)", Toast.LENGTH_LONG).show();
        }
    }

}
