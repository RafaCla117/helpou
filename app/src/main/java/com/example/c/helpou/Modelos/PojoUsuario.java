package com.example.c.helpou.Modelos;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by C on 08/10/2017.
 */

public class PojoUsuario {

    @SerializedName("idUsuario")
    int id_usuario;
    @SerializedName("titulo")
    PojoTitulo titulo_usuario;
    @SerializedName("nombre")
    String nombre_usuario;
    @SerializedName("apPat")
    String ap_pat_usuario;
    @SerializedName("apMat")
    String ap_mat_usuario;
    @SerializedName("correo")
    String email_usuario;
    @SerializedName("pwd")
    String pass_usuario;
    @SerializedName("fechaNacimiento")
    String f_nac_usuario;
    @SerializedName("sexo")
    int sexo_usuario;

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public PojoTitulo getTitulo_usuario() {
        return titulo_usuario;
    }

    public void setTitulo_usuario(PojoTitulo titulo_usuario) {
        this.titulo_usuario = titulo_usuario;
    }

    public String getNombre_usuario() {
        return nombre_usuario;
    }

    public void setNombre_usuario(String nombre_usuario) {
        this.nombre_usuario = nombre_usuario;
    }

    public String getAp_pat_usuario() {
        return ap_pat_usuario;
    }

    public void setAp_pat_usuario(String ap_pat_usuario) {
        this.ap_pat_usuario = ap_pat_usuario;
    }

    public String getAp_mat_usuario() {
        return ap_mat_usuario;
    }

    public void setAp_mat_usuario(String ap_mat_usuario) {
        this.ap_mat_usuario = ap_mat_usuario;
    }

    public String getEmail_usuario() {
        return email_usuario;
    }

    public void setEmail_usuario(String email_usuario) {
        this.email_usuario = email_usuario;
    }

    public String getPass_usuario() {
        return pass_usuario;
    }

    public void setPass_usuario(String pass_usuario) {
        this.pass_usuario = pass_usuario;
    }

    public String getF_nac_usuario() {
        return f_nac_usuario;
    }

    public void setF_nac_usuario(String f_nac_usuario) {
        this.f_nac_usuario = f_nac_usuario;
    }

    public int getSexo_usuario() {
        return sexo_usuario;
    }

    public void setSexo_usuario(int sexo_usuario) {
        this.sexo_usuario = sexo_usuario;
    }

    @Override
    public String toString() {
        return "PojoUsuario{" +
                "id_usuario='" + id_usuario + '\'' +
                ", titulo_usuario=" + titulo_usuario +
                ", nombre_usuario='" + nombre_usuario + '\'' +
                ", ap_pat_usuario='" + ap_pat_usuario + '\'' +
                ", ap_mat_usuario='" + ap_mat_usuario + '\'' +
                ", email_usuario='" + email_usuario + '\'' +
                ", pass_usuario='" + pass_usuario + '\'' +
                ", f_nac_usuario='" + f_nac_usuario + '\'' +
                ", sexo_usuario=" + sexo_usuario +
                '}';
    }
}
