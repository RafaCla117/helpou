package com.example.c.helpou.Modelos;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by C on 06/11/2017.
 */

public class PojoListaProductos {

    @SerializedName("idEstimadoGasto")
    int id_estimado_gasto;
    @SerializedName("idSuperMercado")
    int id_super;
    @SerializedName("fechaLista")
    String fecha_lista;
    @SerializedName("ListaProductos")
    List<PojoProducto> lista_productos = new ArrayList<>();

    public int getId_estimado_gasto() {
        return id_estimado_gasto;
    }

    public void setId_estimado_gasto(int id_estimado_gasto) {
        this.id_estimado_gasto = id_estimado_gasto;
    }

    public int getId_super() {
        return id_super;
    }

    public void setId_super(int id_super) {
        this.id_super = id_super;
    }

    public String getFecha_lista() {
        return fecha_lista;
    }

    public void setFecha_lista(String fecha_lista) {
        this.fecha_lista = fecha_lista;
    }

    public List<PojoProducto> getLista_productos() {
        return lista_productos;
    }

    public void setLista_productos(List<PojoProducto> lista_productos) {
        this.lista_productos = lista_productos;
    }

    @Override
    public String toString() {
        return "PojoListaProductos{" +
                "id_estimado_gasto=" + id_estimado_gasto +
                ", id_super=" + id_super +
                ", fecha_lista='" + fecha_lista + '\'' +
                ", lista_productos=" + lista_productos +
                '}';
    }
}
