package com.example.c.helpou.Fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.c.helpou.Fragments.FragmentsInterfaces.ArtMasCompInterface;
import com.example.c.helpou.Modelos.PojoIdUsuario;
import com.example.c.helpou.R;
import com.example.c.helpou.Retrofit.RestApi;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class ArtMasCompFragmen extends Fragment implements ArtMasCompInterface {

    private static final String TAG = "TAG - ArtMasCompFragmen";
    private PieChart jv_pie_chart;
    private View fragmentView;
    private int id_usuario = 1;
    private String datos_user;
    private ArrayList<Float> no_veces_comprado = new ArrayList<>();
    private ArrayList<String> nombre_articulo = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_art_mas_comp, container, false);

        jv_pie_chart = fragmentView.findViewById(R.id.xml_pie_chart_art_mas_comprado);

        RestApi caller_service = new RestApi(getContext());

        ArrayList<String> stringArray = new ArrayList<>();

        JSONArray jsonArray;

        try {
            jsonArray = new JSONArray(datos_user);

            for (int i = 0; i < jsonArray.length(); i++) {
                stringArray.add(jsonArray.getString(i));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        id_usuario = (int) Float.parseFloat(stringArray.get(0));

        PojoIdUsuario pojo_id_us = new PojoIdUsuario();
        pojo_id_us.setId_usuario(id_usuario);

        caller_service.getListArtMasComprado(pojo_id_us, this);

        return fragmentView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        RestApi caller_service = new RestApi(getContext());

        Log.i(TAG, " onCreateView() salida de json: " + datos_user);

        ArrayList<String> stringArray = new ArrayList<>();

        JSONArray jsonArray;

        try {
            jsonArray = new JSONArray(datos_user);

            for (int i = 0; i < jsonArray.length(); i++) {
                stringArray.add(jsonArray.getString(i));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        id_usuario = (int) Float.parseFloat(stringArray.get(0));

        PojoIdUsuario pojo_id_us = new PojoIdUsuario();
        pojo_id_us.setId_usuario(id_usuario);

        caller_service.getListArtMasComprado(pojo_id_us, this);

    }

    @Override
    public void onStart() {
        super.onStart();
        RestApi caller_service = new RestApi(getContext());

        Log.i(TAG, " onCreateView() salida de json: " + datos_user);

        ArrayList<String> stringArray = new ArrayList<>();

        JSONArray jsonArray;

        try {
            jsonArray = new JSONArray(datos_user);

            for (int i = 0; i < jsonArray.length(); i++) {
                stringArray.add(jsonArray.getString(i));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        id_usuario = (int) Float.parseFloat(stringArray.get(0));

        PojoIdUsuario pojo_id_us = new PojoIdUsuario();
        pojo_id_us.setId_usuario(id_usuario);

        caller_service.getListArtMasComprado(pojo_id_us, this);
    }

    @Override
    public void onResume() {
        super.onResume();
        RestApi caller_service = new RestApi(getContext());

        Log.i(TAG, " onCreateView() salida de json: " + datos_user);

        ArrayList<String> stringArray = new ArrayList<>();

        JSONArray jsonArray;

        try {
            jsonArray = new JSONArray(datos_user);

            for (int i = 0; i < jsonArray.length(); i++) {
                stringArray.add(jsonArray.getString(i));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        id_usuario = (int) Float.parseFloat(stringArray.get(0));

        PojoIdUsuario pojo_id_us = new PojoIdUsuario();
        pojo_id_us.setId_usuario(id_usuario);

        caller_service.getListArtMasComprado(pojo_id_us, this);
    }

    public void setDatos_user(String datos_user) {
        this.datos_user = datos_user;
    }

    public void makeGraph() {

        //Propiedades de la grafica
        jv_pie_chart.setUsePercentValues(false);
        jv_pie_chart.getDescription().setEnabled(false);
        jv_pie_chart.setExtraOffsets(5, 10, 5, 5);
        jv_pie_chart.setDragDecelerationFrictionCoef(0.95f);
        jv_pie_chart.setDrawHoleEnabled(true);
        jv_pie_chart.setHoleColor(Color.WHITE);
        jv_pie_chart.setTransparentCircleRadius(61f);
        jv_pie_chart.animateY(1000, Easing.EasingOption.EaseInOutCubic);

        ArrayList<PieEntry> yValues = new ArrayList<>();

        for (int i = 0; i < no_veces_comprado.size(); i++) {
            yValues.add(new PieEntry(no_veces_comprado.get(i), nombre_articulo.get(i)));
        }

        PieDataSet data_set = new PieDataSet(yValues, "Productos");
        data_set.setSliceSpace(3f);
        data_set.setSliceSpace(5f);
        data_set.setColors(ColorTemplate.JOYFUL_COLORS);

        PieData pie_data = new PieData(data_set);
        pie_data.setValueTextSize(10f);
        pie_data.setValueTextColor(Color.BLACK);

        jv_pie_chart.setData(pie_data);

        no_veces_comprado.clear();
        nombre_articulo.clear();
    }

    @Override
    public void listaArtMasComprados(String list_artc) {

        list_artc = list_artc.replace("[", "");
        list_artc = list_artc.replace("]", "");
        list_artc = list_artc.replace(" ", "");

        Log.i(TAG, "listaArtMasComprados: articulos mas comprados: " + list_artc);
        String[] aux_array = list_artc.split(",");

        for (int i = 0; i < aux_array.length; i++) {
            if (i % 2 == 0) {
                nombre_articulo.add(aux_array[i]);
            } else {
                no_veces_comprado.add(Float.parseFloat(aux_array[i]));
            }
        }

        makeGraph();
    }


}

