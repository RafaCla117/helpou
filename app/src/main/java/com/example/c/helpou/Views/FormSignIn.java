package com.example.c.helpou.Views;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.c.helpou.Modelos.PojoDireccion;
import com.example.c.helpou.Modelos.PojoTitulo;
import com.example.c.helpou.Modelos.PojoUsuario;
import com.example.c.helpou.R;
import com.example.c.helpou.Retrofit.RestApi;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FormSignIn extends AppCompatActivity {

    String TAG = "TAG";
    private static final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[_-]).{6,20})";
    EditText jtvNombreUs, jtvApPatUs, jtvApMatUs, jtvEmailUs, jtvContraseñaUs, jtvRepitContrUs;
    TextView jv_tv_born_date;
    RadioButton jv_rd_btn_male, jv_rd_btn_female;
    String sNombreUs, sApPatUs, sApMatUs, sEmailUs, sContraseñaUs, sRepitContrUs;
    int sexoUs;
    private Calendar calendar;
    private int year, month, day;
    StringBuilder fecha_nac_usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_sign_in);

        jtvNombreUs = (EditText) findViewById(R.id.xmlNombreUs);
        jtvApMatUs = (EditText) findViewById(R.id.xmlApMatUs);
        jtvApPatUs = (EditText) findViewById(R.id.xmlApPatUs);
        jtvEmailUs = (EditText) findViewById(R.id.xmlEmailUs);
        jtvContraseñaUs = (EditText) findViewById(R.id.xmlContraseñaUs);
        jtvRepitContrUs = (EditText) findViewById(R.id.xmlRepitContraseñaUs);
        jv_tv_born_date = (TextView) findViewById(R.id.xml_tv_born_date);
        jv_rd_btn_male = (RadioButton) findViewById(R.id.xml_rd_btn_male);
        jv_rd_btn_female = (RadioButton) findViewById(R.id.xml_rd_btn_female);

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(year, month + 1, day);


    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this, myDateListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
            // arg1 = year
            // arg2 = month
            // arg3 = day
            showDate(arg1, arg2 + 1, arg3);
        }
    };

    private void showDate(int year, int month, int day) {
        fecha_nac_usuario = new StringBuilder().append(day).append("/").append(month).append("/").append(year);
        jv_tv_born_date.setText(fecha_nac_usuario);
    }

    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);
    }

    public void registrarUsuario(View v) {

        boolean flag = false;

        sNombreUs = "" + jtvNombreUs.getText();
        sApPatUs = "" + jtvApPatUs.getText();
        sApMatUs = "" + jtvApMatUs.getText();
        sEmailUs = "" + jtvEmailUs.getText();
        sContraseñaUs = "" + jtvContraseñaUs.getText();
        sRepitContrUs = "" + jtvRepitContrUs.getText();

        if (jv_rd_btn_male.isChecked()) {
            sexoUs = 1;
            jv_rd_btn_male.setChecked(false);
            flag = true;
        } else if (jv_rd_btn_female.isChecked()) {
            sexoUs = 0;
            jv_rd_btn_female.setChecked(false);
            flag = true;
        }

        PojoTitulo titulo_usuario = new PojoTitulo();

        titulo_usuario.setId_titulo(0);
        titulo_usuario.setNombre_titulo("Iniciado");
        titulo_usuario.setDescripcion_titulo("Lv-0");


        PojoUsuario nuevoUsuario = new PojoUsuario();

        nuevoUsuario.setId_usuario(0);
        nuevoUsuario.setTitulo_usuario(titulo_usuario);
        nuevoUsuario.setNombre_usuario(sNombreUs);
        nuevoUsuario.setAp_pat_usuario(sApPatUs);
        nuevoUsuario.setAp_mat_usuario(sApMatUs);
        nuevoUsuario.setEmail_usuario(sEmailUs);
        nuevoUsuario.setPass_usuario(sContraseñaUs);
        nuevoUsuario.setF_nac_usuario("" + fecha_nac_usuario);
        nuevoUsuario.setSexo_usuario(sexoUs);

        if (sContraseñaUs.equals(sRepitContrUs) && flag) {

            if (validateDataUser(nuevoUsuario)) {

                RestApi restApi = new RestApi(this);

                restApi.insertaUsuario(nuevoUsuario);

                jtvNombreUs.setText("");
                jtvApPatUs.setText("");
                jtvApMatUs.setText("");
                jtvEmailUs.setText("");
                jtvContraseñaUs.setText("");
                jtvRepitContrUs.setText("");
                jv_tv_born_date.setText("");
            }

        } else {

            if (flag){
                Log.i(TAG, "validateDataUser: User password doesn't match");
                Toast toast1 = Toast.makeText(getApplicationContext(), "Las contraseñas no coinciden porfavor verifica  ;)", Toast.LENGTH_SHORT);
                toast1.show();
            }else {
                Toast toast1 = Toast.makeText(getApplicationContext(), "Selecciona tu genero  ;)", Toast.LENGTH_SHORT);
                toast1.show();
            }



        }


    }

    public static boolean validateEmail(String email) {

        // Compiles the given regular expression into a pattern.
        Pattern pattern = Pattern.compile(PATTERN_EMAIL);

        // Match the given input against this pattern
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();

    }

    /**
     * Valida un string si no contiene caracteres especiales
     * o si contiene que sean los que estan permitidos
     *
     * @param s
     * @return
     */
    public boolean validateString(String s) {

        if (s == null || s.trim().isEmpty()) {
            Log.i(TAG, "validateString: Incorrect format of string");
            return false;
        }

        Pattern p = Pattern.compile("^[\\p{L} .-]+$");
        Matcher m = p.matcher(s);

        boolean b = m.find();

        if (b == true)
            Log.i(TAG, "validateString: there is a valid string");
        else
            Log.i(TAG, "validateString: there isn't a valid sting");

        return b;
    }

    public boolean validatePassword(String password) {

        if (password == null || password.trim().isEmpty()) {
            Log.i(TAG, "getSpecialCharacterCount: Incorrect format of string");
            return false;
        }

        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password);

        if (matcher.matches()) {
            Log.i(TAG, "validatePassword contraseña valida :) ");
            return true;
        } else {
            Log.i(TAG, "validatePassword: Contraseña no valida");
            return false;
        }

    }

    public boolean validateDataUser(PojoUsuario pojoUsuario) {

        if (!validateString(pojoUsuario.getNombre_usuario())) {
            Log.i(TAG, "validateDataUser: User name invalid");
            Toast toast1 = Toast.makeText(getApplicationContext(), "Nombre de usuario no valido porfavor verifica  ;)", Toast.LENGTH_LONG);
            toast1.show();
            return false;
        }

        if (!validateString(pojoUsuario.getAp_pat_usuario())) {
            Log.i(TAG, "validateDataUser: User father last name invalid");
            Toast toast1 = Toast.makeText(getApplicationContext(), "Apellido paterno no valido porfavor verifica  ;)", Toast.LENGTH_LONG);
            toast1.show();
            return false;
        }

        if (!validateString(pojoUsuario.getAp_pat_usuario())) {
            Log.i(TAG, "validateDataUser: User father last name invalid");
            Toast toast1 = Toast.makeText(getApplicationContext(), "Apellido paterno no valido porfavor verifica  ;)", Toast.LENGTH_LONG);
            toast1.show();
            return false;
        }

        if (!validateString(pojoUsuario.getAp_mat_usuario())) {
            Log.i(TAG, "validateDataUser: User mother last name invalid");
            Toast toast1 = Toast.makeText(getApplicationContext(), "Apellido materno no valido porfavor verifica  ;)", Toast.LENGTH_LONG);
            toast1.show();
            return false;
        }

        if (!validateEmail(pojoUsuario.getEmail_usuario())) {
            Log.i(TAG, "validateDataUser: User E-mail invalid");
            Toast toast1 = Toast.makeText(getApplicationContext(), "Correo no valido porfavor verifica  ;)", Toast.LENGTH_LONG);
            toast1.show();
            return false;
        }

        if (!validatePassword(pojoUsuario.getPass_usuario())) {
            Log.i(TAG, "validateDataUser: User password invalid");
            Toast toast1 = Toast.makeText(getApplicationContext(), "Contraseña no valida porfavor verifica  ;)", Toast.LENGTH_LONG);
            toast1.show();
            return false;
        }

        return true;
    }

}