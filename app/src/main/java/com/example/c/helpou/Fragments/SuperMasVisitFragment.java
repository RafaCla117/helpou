package com.example.c.helpou.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.c.helpou.R;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class SuperMasVisitFragment extends Fragment {

    View fragment_view;
    BarChart jv_bar_chart_super_most_visit;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragment_view = inflater.inflate(R.layout.fragment_super_mas_visit, container, false);

        jv_bar_chart_super_most_visit = fragment_view.findViewById(R.id.xml_bar_chart_super_most_visit);

        makeGraph();

        return fragment_view;
    }

    private void makeGraph() {
        //Propiedades de la grafica
        jv_bar_chart_super_most_visit.getDescription();
        jv_bar_chart_super_most_visit.animateY(1000, Easing.EasingOption.EaseInOutCubic);

        ArrayList<BarEntry> yValues = new ArrayList<>();

        yValues.add(new BarEntry(34f, 0));
        yValues.add(new BarEntry(45f, 1));

        BarDataSet data_set = new BarDataSet(yValues, "Supermercado");
        data_set.setColors(ColorTemplate.JOYFUL_COLORS);

        ArrayList<String> title_dates = new ArrayList<>();
        title_dates.add("Walmart");
        title_dates.add("Chedraui");

        BarData bar_data = new BarData(data_set);
        bar_data.setValueTextSize(10f);

        jv_bar_chart_super_most_visit.setData(bar_data);
    }
}
