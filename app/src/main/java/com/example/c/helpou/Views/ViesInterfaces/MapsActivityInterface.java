package com.example.c.helpou.Views.ViesInterfaces;

import com.example.c.helpou.Modelos.PojoCentroComercial;

import java.util.List;

/**
 * Created by C on 07/11/2017.
 */

public interface MapsActivityInterface {
    /**
     * Este metodo lo mandamos a llamar cuando pintamos los supermercados
     * que nos manda nuestro servidor
     * @param list_centros es una lista con datos de los centros comerciales
     *                     que tenemos almacenados en el servidor
     */
    void paintSuper(List<PojoCentroComercial> list_centros);
}
